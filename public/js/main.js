$.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
$(document).ready(function() {
	/* Init Datatables */
	$('#dataTbl').DataTable({
		"iCookieDuration": 60*10, // in second // Aku set 10 menit saja
	});

	$('#select-all').on('click', function(){
     if(this.checked) { $(':checkbox').each(function() {this.checked = true; });  } else { $(':checkbox').each(function() {this.checked = false; }); }
	});

   $(document).on('click', '[data-toggle="ajaxModal"]', function(e) {
		e.preventDefault();
		$('#ajaxModal').remove();
		var $this = $(this)
		  , $remote = $this.data('remote') || $this.attr('href')
		  , $modal = $('<div class="modal fade" id="ajaxModal"></div>');
		$('body').append($modal);
		// $modal.modal({backdrop: 'static', keyboard: false});
		$modal.modal();
		$modal.load($remote);	 
		return false; 
	});

});
function _reload_datatables(){
	$('#dataTbl').DataTable({
		"iCookieDuration": 60*10, // in second // Aku set 10 menit saja
		"bDestroy":true
	});
}
function showError(){alert('Error Occurred : Ajax Error or Form action did not exist!'); return false;}
function showConfirmDelete(){
	if (!confirm("Are you sure to delete selected item(s)"))
    {
        return false;
    } 
}
function showResponse(data) { 
    var $formElm = $("#ajxForm");
	var $elModal = $("#ajaxModal");
	var $errElm = $("#ajxForm_message");	
	$errElm.show().html('');
    if(!$.isEmptyObject(data.error)){
		if($elModal.length) {
			/* bila ada modal */
			$errElm = $("#modal-message");
			$errElm.show().html('');
		}
		
		/* Gagal */
		var msg = data.error; 
		var x = "";; 
		$.each( msg, function( key, value ) {
			x = x + '<li>' + value + '</li>';
		});
		if ($errElm.length){$errElm.append('<div class="alert alert-danger"><ul>'+x+'</ul></div>');}
	} else {
		var msg = data.message; 
		$.each( msg, function( key, value ) {	
			if ($errElm.length){$errElm.append('<div class="alert alert-success">'+value+'</div>');}
		});
		
		/* bool - form reset */
		if($formElm.length && $formElm.attr('data-ajxForm-reset')!="false"){
			$formElm[0].reset();
		}
		
		/* Bila ada datatables */
		if($("#dataTbl").length != 0){		
			// $('#dataTbl').DataTable({"bDestroy":true });
			_reload_datatables();
		}		
		
		/* Bila ada modal - Hide */
		if($elModal.length) {			
			//$elModal.modal('hide').data( 'bs.modal', null );;
			$elModal.remove();
			$('.modal-backdrop').remove();
			$('body').removeClass("modal-open");
		}	
		
	}
	$errElm.delay(4000).fadeOut('slow');
}
