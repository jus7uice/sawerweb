<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Post;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Post::class, function (Faker $faker) {
    $video_url = "https://www.youtube.com/watch?v=T04yiCoN41A";
	$hashed_id = $faker->ean13().$faker->ean13();
	
    return [
		'hashed_id' => $hashed_id,
		'title' => $faker->sentence($nbWords = 6, $variableNbWords = true),
		'user_id' => $faker->numberBetween($min = 1, $max = 20),
		// 'user_id' => 5,
		'category_id' => $faker->randomDigit(),
		'video_url' => $video_url,
		'description' => $faker->text($maxNbChars = 200),
		'http_headers' => json_encode(Request()->header()),
	
    ];
});
