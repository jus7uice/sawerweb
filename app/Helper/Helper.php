<?php

namespace App\Helper;
use DB;

class Helper
{
	static function db_to_json($query_obj, $filename, $reform_arr = true) {
		if ($reform_arr) {
			$res = array();
			foreach ($query_obj as $row) {
				$res[] = $row;
			}
		}else{
			$res = $query_obj;
		}
		$json = json_encode($res);
		Helper::file_write($filename, $json);
	}
	
	static function file_write($file, $content = "") {
		file_put_contents($file, $content);
	}
	
	static function file_read($file) {
		if(!file_exists($file)){
			file_put_contents($file, "");
			chmod($file, 0777);
		}else{
			return file_get_contents($file);
		}
	}
}