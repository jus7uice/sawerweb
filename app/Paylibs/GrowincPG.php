<?php
namespace App\Paylibs;
use DB;
use Curl;

use App\PaymentProvider;

class GrowincPG
{
	function getToken(){
		
		if($data = PaymentProvider::where('module_name','GrowincPG')->first())
		{
			$params = [
				'grant_type' => 'client_credentials',
				'client_id'  => $data->client_id,
				'client_secret' => $data->client_secret,
			];

			/* 1 GET Access. Let log user in ( one time only for all next requests ) */
			$response = Curl::to($data->api_url.'api/oauth/token')
			->withData($params)
			->asJson()
			->post();
			
			return $response;
			// die(debug($response));
		}
		
		die('getToken');
	}
	
}