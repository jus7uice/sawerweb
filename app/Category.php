<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
	use SoftDeletes;
	protected $table = 'category';
	protected $guarded = [];
	
	//each category might have one parent
	public function parent() {
		return $this->belongsTo(static::class, 'parent_id');
	}

	//each category might have multiple children
	public function children() {
		return $this->hasMany(static::class, 'parent_id')->orderBy('name', 'asc');
	}
  
}
