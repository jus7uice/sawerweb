<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardCtr extends Controller
{
    /* Index */
	function index(Request $request){
		return view('dashboard');
	}
}
