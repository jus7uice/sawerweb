<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use DB;
use Str;
use Validator;
use Illuminate\Validation\Rule;
use Datatables;

use LaravelVideoEmbed;
use Jenssegers\Agent\Agent;


use App\User;
use App\Post;
use App\Category;
use App\PostTag;

class UserVideoCtr extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		
		return view('akun_video');
    }
	
	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getData(Request $request)
    {	
		$user = Auth::guard('member')->user();
		# Load User Video
		$rows = Post::with('category','user')->where('user_id',$user->id);
        return Datatables::of($rows)
		->addColumn('chkbox',function($row){
			return '<input type="checkbox" name="deleteItems[]" value="'.$row->id.'" />';
		})
		->addColumn('status',function($row){
			if($row->status == 1)	return '<span class="label label-success">Active</span>';
			else return '<span class="label label-danger">Not Active</span>';
		})
		->addColumn('action',function($row){
			$action = '
				<a href="'.url('akun/video/ubah?uuid='.$row->uuid).'">Ubah</a>
			
			';
			return $action;
		})
		->addColumn('lbl_created_at',function($row){
			return date("Y-m-d H:i:s",strtotime($row->created_at));
		})
		->rawColumns(['chkbox','lbl_post_tag','lbl_created_at','status','action'])
		->make(true);
    }
	
	
	/**
     * Create a resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCreate(Request $request)
    {
		$category = Category::where('status','<',2)->pluck('name','id')->toArray();
		$categoryList = [' '=>'Pilih Kategori :'] + $category;
		return view('akun_video_create',compact('categoryList'));
    }
	
	/**
     * Create a resource : POST.
     *
     * @return \Illuminate\Http\Response
     */
    public function postCreate(Request $request)
    {	
		$user = Auth::guard('member')->user();
		/* Validate */
		$request->validate([
			'title' => 'required|unique:posts,title',
			'category_id' => 'required',
			'video_url' => 'required',
			'description' => 'required',
		]);		
		
		/* Call Agent */
		$agent = new Agent();		
		
		/* Save to DB */
		$row = new Post;
		$row->fill($request->all());
		$row->uuid = Str::uuid();
		$row->user_id = $user->id;
		$row->description = str_encode($request->description);
		
		$row->http_headers = json_encode($request->header());
		$row->ua_flatform = $agent->platform();
		$row->ua_browser = $agent->browser();
		$row->ip = $request->ip();		
		$row->save();
		
		// if(is_array($request->tag) && count($request->tag) > 0){
			// $data = [];
			// foreach($request->tag as $tag){
				// $data[] = ['user_video_id'=>$row->id,'tag_id'=>$tag];
			// }
			// PostTag::insert($data);
		// }
				
		/* Redirc */
		return redirect()->back()->with('msg',trans('message.save.success'));
    }
	
	/**
     * Edit a resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getEdit(Request $request)
    {
		$user = Auth::guard('member')->user();
		$request->validate([
			'uuid' => 'required',
		]);
		
		if(!$data = Post::where('user_id',$user->id)->where('uuid',$request->uuid)->first()) return redirect('akun/video')->withErrors(['Data tidak ditemukan']);
		
		
		$category = Category::where('status','<',2)->pluck('name','id')->toArray();
		$categoryList = [' '=>'Select Category :'] + $category;
		
		// $postTag = PostTag::with('tag')->where('user_video_tags.post_id',$request->id)->get();
		$PostTagList = [];
		// foreach($postTag as $row){
			// $PostTagList = $PostTagList + [$row->tag_id => $row->tag->name];
		// }
		
		// die(debug($PostTagList));
		return view('akun_video_edit',compact('data','categoryList','PostTagList'));
    }
	
	
	/**
     * Edit a resource : POST.
     *
     * @return \Illuminate\Http\Response
     */
    public function postEdit(Request $request)
    {
		$user = Auth::guard('member')->user();
		
		/* Validate */
		$request->validate([
			'title' => [
				'required',
				Rule::unique('posts')->ignore($request->uuid,'uuid'),
			],
			'video_url' => [
				'required',
				Rule::unique('posts')->ignore($request->uuid,'uuid'),
			],
			'category_id' => 'required',
			'description' => 'required',
		]);
				
		/* Save to DB */
		$row = Post::where('user_id',$user->id)->where('uuid',$request->uuid)->first();
		$row->fill($request->all());
		$row->description = str_encode($request->description);
		$row->save();
				
		// if(is_array($request->tag) && count($request->tag) > 0){
			// DB::table('user_video_tags')->where('user_video_id',$request->id)->delete();
			// $data = [];
			// foreach($request->tag as $tag){
				// $data[] = ['user_video_id'=>$row->id,'tag_id'=>$tag];
			// }
			// PostTag::insert($data);
		// }

		/* Redirc */
		// return redirect()->back()->with('msg',trans('message.save.success'));
		return redirect('akun/video')->with('msg',trans('message.save.success'));
    }
	
}
