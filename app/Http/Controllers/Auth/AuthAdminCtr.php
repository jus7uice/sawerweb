<?php

namespace App\Http\Controllers\Auth;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Route;
use DB;
use App\Throttle;

class AuthAdminCtr extends Controller
{
 
	function index(Request $request){
		/* Check Throttle */
		$isThottle = 0;
		if(Throttle::where('ip',$request->ip())->where('end_time','>',time())->where('status',1)->first()){
			$isThottle = 1;
			$request->session()->put('throttle',0);		/* Reset Counter, biar habis ini bisa coba 3x lagi */	
			$request->session()->put('attempt_lists','');
		}
		return view('backend.login',compact('isThottle'));
	}
	
	/**
     * Admin Login (POST) Handling
     *
	 * @param  Request  $request
     * @return Response
     */
	function postAuth(Request $request)
	{
		/* Validate */
		$request->validate([
			'username' => 'required|min:3|max:255',
			'password' => 'required|min:5',
		]);
		
		/* AUTH MAKE */
		if (Auth::guard('admin')->attempt(['username' => $request->username, 'password' => $request->password])) {
			/* Authentication passed... */
			/* Session for 1 device login */
			$login_session =  md5(time());
			session(['login_session' => $login_session]);
			DB::table('admin')->where(['username' => $request->username])->update(['login_session'=>$login_session]);
			/* Redirect */
			return redirect()->intended(ADMIN_PATH.'dashboard');
		} else {
			
			return redirect('/')->withErrors(trans('auth.failed'));
		}		
	}
	
	/**
     * Admin Login (POST) Handling With Throttle
     *
	 * @param  Request  $request
     * @return Response
     */
	function postAuthWithThrottle(Request $request)
	{
		/* Validate */
		$request->validate([
			'username' => 'required|min:3|max:255',
			'password' => 'required|min:5',
		]);
		
		/* Check Throttle */
		if(Throttle::where('ip',$request->ip())->where('end_time','>',time())->where('status',1)->first()){
			return redirect(ADMIN_PATH)->withErrors(trans('auth.throttle',["seconds"=>60]));
		}
		
		/* AUTH MAKE */
		if (Auth::guard('admin')->attempt(['username' => $request->username, 'password' => $request->password])) {
		// Authentication passed...
			
			/* Valid */
			$throttle = DB::table('throttle')->where('ip',$request->ip())->where('session',csrf_token())->update(['status'=>0]);	
			$request->session()->put('throttle',0);		/* Reset Counter */	
			$request->session()->put('attempt_lists','');
			return redirect()->intended(ADMIN_PATH.'dashboard');
		} else {
			if(!$request->session()->has('throttle')){
				$request->session()->put('throttle',0);
			}
			if(!$request->session()->has('attempt_lists')){
				$request->session()->put('attempt_lists','');
			}
			$request->session()->put('throttle',session('throttle')+1);				
			$request->session()->put('attempt_lists',session('attempt_lists').($request->username.':'.$request->password));
			
			if(session('throttle')>=3){
				$attempt_lists = session('attempt_lists');
				/* Create Throttle */
				Throttle::updateOrCreate(
					[
						'ip' => $request->ip(),
						'status' => 1
					],[
						'session' => csrf_token(),
						'try' => DB::raw('try + '.session('throttle')),
						// 'attempt_lists' => json_encode([$request->usename=>$request->password]),
						// 'attempt_lists' => DB::raw('attempt_lists + '.session('attempt_lists')),
						// 'attempt_lists' => DB::raw("CONCAT_WS('|',attempt_lists,'$attempt_lists')"),
						'start_time' => time(),
						'end_time' => time()+60,
					]
				);		
				DB::update("update throttle set attempt_lists = CONCAT_WS('|',attempt_lists,'$attempt_lists') where ip = '".$request->ip()."' AND status = 1");				
			}
			/* + separator pada attempt_lists  */
			$request->session()->put('attempt_lists',session('attempt_lists').'|');			
			return redirect(ADMIN_PATH)->withErrors(trans('auth.failed'));
		}
		// if(Auth::guard('admin')->check()){
			// session(['authAdmin' => Auth::guard('admin')->user()]);
			// return session('authAdmin');
		// }
	}
	
	/**
     * Logout page.
     *
     * @return Redirect
     */
    public function logout(Request $request)
    {
        Auth::logout();
		Auth::guard('admin')->logout();
		session()->flush();
        return redirect(ADMIN_PATH)->with('msg',trans('message.logout_success'));
    }



    function foo(Request $request)
    {

    	$routes = Route::getRoutes();
		 // dd($routes);
		  $results = array();
		  foreach ($routes as $route)
		  {
		   // $results[] = $route->uri;
		   $results[] = [
				'uri' => $route->uri,
				'methods' => $route->methods[0],
			];
		  }
		  sort($results);
		 return response()->json($results);

    }

}
