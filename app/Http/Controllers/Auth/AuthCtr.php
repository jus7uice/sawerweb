<?php

namespace App\Http\Controllers\Auth;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Route;
use DB;
use App\User;
use App\Throttle;

class AuthCtr extends Controller
{
 
	function index(Request $request){
		/* Check Throttle */
		$isThottle = 0;
		if(Throttle::where('ip',$request->ip())->where('end_time','>',time())->where('status',1)->first()){
			$isThottle = 1;
			$request->session()->put('throttle',0);		/* Reset Counter, biar habis ini bisa coba 3x lagi */	
			$request->session()->put('attempt_lists','');
		}
		return view('backend.login',compact('isThottle'));
	}
	
	/**
     * User Login (POST) Handling
     *
	 * @param  Request  $request
     * @return Response
     */
	 
	function postLogin(Request $request)
	{
		/* Validate */
		$request->validate([
			'email' => 'required|email',
			'password' => 'required|string|min:8',
		]);
		
		/* AUTH MAKE */
		if (Auth::guard('member')->attempt(['email' => $request->email, 'password' => $request->password])) {
			/* Authentication passed... */
			
			/* Bila sudah aktivasi */
			$email_verified_at = User::where('email',$request->email)->value('email_verified_at');
			if(strlen($email_verified_at) <= 10){
				Auth::guard('member')->logout();
				return redirect('/login')->withErrors("Email atau Password tidak benar!");
			}				
			
			/* Session for 1 device login */
			$login_session =  md5(time());
			session(['login_session_member' => $login_session]);
			User::where(['email' => $request->email])->update(['login_session'=>$login_session]);
			/* Redirect */
			return redirect()->intended('/');
		} else {
			
			return redirect('/login')->withErrors("Email atau Password tidak benar!");
		}		
	}	
	
	/**
     * Logout page.
     *
     * @return Redirect
     */
    public function logout(Request $request)
    {
        Auth::logout();
		Auth::guard('member')->logout();
		session()->flush();
        return redirect("/login")->with(['msg' => 'Logout sukses :)']);
    }



}
