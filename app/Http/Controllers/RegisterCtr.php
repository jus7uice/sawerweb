<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Mail;
use Hash;
use Str;
use Agent;

use App\User;


class RegisterCtr extends Controller
{
	/* 
		Get : Form Fegistrasi
	*/
	function getRegister(){
		if (Auth::guard('member')->check())
		{
			return redirect('/');
		}
		
		
		return view('register');
	}
	
	/* 
		POST: Registrasi
	*/
	function postRegister(Request $request){
		$request->validate([
			'email' => 'required|unique:users|max:255',
			'name' => 'required',
			'username' => 'required|unique:users|max:255|min:4|alpha_num',
			'password' => 'required|string|min:8|confirmed',
		]);
		
		/* Passed */
		
		/* Save to DB */
		$uuid = Str::uuid();
		$verification_code = md5(date('H:i:s')).md5(rand(1,99).date("Y"));
		$link_aktivasi = url('/aktivasi?uid='.$uuid.'&verification_code='.$verification_code);

		$data_save = [
			'uuid' => $uuid,
			'name' => $request->name,
			'username' => $request->username,
			'email' => $request->email,
		   
			'password' => Hash::make($request->password),		
			'http_headers' => json_encode(Request()->header()),
			'ua_flatform' => Agent::platform(),
			'ua_browser' => Agent::browser(),

			'ip' => $request->ip(),
			'remember_token' => Str::random(10),
			'verification_code' => $verification_code,			
		];
		
			
		User::create($data_save); debug($data_save);	
		
		$param = [
			'title' => "Aktivasi Akun ".$request->email,
			'recepient_name' => $request->name,
			'content' => '
				<p>Selamat bergabung di Sawer.id, Untuk mengaktifkan akun anda, silahkan klik link aktivasi dibawah ini :</p>
				<p><center><a href="'.$link_aktivasi.'" target="_aktivasi" rel="aktivasi" style="display: inline-block; border-radius: 5px; padding: 20px 40px; background-color: #aacc00; color: #fafafa"> Konfirmasi Aktivasi </a> </center></p>
				<p>Atau salin link url berikut ini pada dibrowser anda. 
				<br />Link Url Aktivasi : <a href="#">'.$link_aktivasi.'</a>
				</p>
			',
			
		];
		
		// return view('emails.email', compact('param'));
		// Mail::send('emails.email', compact('param'), function ($m) use ($request) {
			// $m->from(EMAIL_SENDER,EMAIL_SENDER_NAME);
			// $m->to($request->email,$request->name);
			// $m->subject("Aktivasi Akun ".$request->email);
		// });
		
		return redirect()->intended('login')->with(['email_verification'=>'1']);
	}
	
	/* 
		GET :  do Aktivasi
	*/
	function doAktivasi(Request $request){
		
		if($data = User::where('uid',$request->uid)->where('verification_code',$request->verification_code)->first()){
			User::where('uid',$request->uid)->where('verification_code',$request->verification_code)->whereNull('email_verified_at')->update(['email_verified_at'=> date("Y-m-d H:i:s")]);
		}
		
		
		return view('aktivasi',compact('data'));
	}
	
	
	
}
