<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Post;
use App\StickerStock;

class VideoCtr extends Controller
{
	function index(Request $request){
		$data = Post::paginate(16);
		
		return view('video',compact('data'));		
	}
	
	
	function getVideo(Request $request, $video_id){
		if($data = Post::with('user')->where('uuid',$video_id)->first()){
			
			return view('video_view',compact('data'));
		}
		
		return redirect(url('/'));
	}
}
