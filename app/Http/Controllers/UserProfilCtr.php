<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Illuminate\Validation\Rule;
use Image;

use App\User;

class UserProfilCtr extends Controller
{
	
	/* di overide ke {domain}/{username} */
   /*  function index(){
		$user = User::with('organizer')->find(Auth::guard('member')->user()->id);
		
		// debug($user);
		
		return view('user_profil',compact("user"));
	} */
	
	function getEdit(){
		$user = User::find(Auth::guard('member')->user()->id);
		return view('akun_profil_edit',compact("user"));
	}
	
	function postEdit(Request $request){
		
		$user = User::find(Auth::guard('member')->user()->id);
		
		/* Validate */
		$request->validate([
			'username' => [
				'required',
				Rule::unique('users')->ignore($user->id),
				'max:255','min:4','alpha_num'
			],
			'name' => 'required|min:4',
			
		]);
		
		// files storage folder
		$dir = UPLOAD_PATH.'images/'.date('Ymd');	
		$big_file_name = $request->photo;
		$med_file_name = ($request->photo!="")?str_replace('.','_m.',$request->photo):"";
		$small_file_name = ($request->photo!="")?str_replace('.','_s.',$request->photo):"";
			
		if ($request->hasFile('file'))
		{
			/* MKDIR */
			if (!file_exists($dir)) {mkdir($dir, 0777, true);}
			/* SET DIR */
			$dir = $dir.'/';
									
			/* START UPLOAD */
			$path = $request->file->path();
			$file_name = md5($request->file->getClientOriginalName().time());
			$extension = strtolower($request->file->getClientOriginalExtension());
			$big_file_name = $dir.$file_name.'.'.$extension;
			$med_file_name = $dir.$file_name.'_m.'.$extension;
			$small_file_name = $dir.$file_name.'_s.'.$extension;
			
			if ($extension == 'png'|| $extension == 'jpg' || $extension == 'bmp' || $extension == 'gif' || $extension == 'jpeg'|| $extension == 'pjpeg')
			{
				// debug('1. intervention');
				$big_img = Image::make($path);
				$big_img->widen(800, function ($constraint) {$constraint->upsize();});				
				$big_img->save($big_file_name, 90);	// save to local is success
							
				// image upload			
				$thumb_img = Image::make($path);
				$thumb_img->fit(400);
				$thumb_img->save($med_file_name, 70);// save file
				
				// image upload			
				$thumb_img = Image::make($path);
				$thumb_img->fit(150);
				$thumb_img->save($small_file_name, 70);// save file
			}
		}
		
		
		
		$user->fill($request->all());
		$user->photo_small_url = $small_file_name;
		$user->photo_medium_url = $med_file_name;
		$user->photo_big_url = $big_file_name;
		$user->save();
		
		
		// debug($request->all());
		return redirect($user->username)->with(['msg'=>'Profil berhasil diperbarui']);
		
	}
}
