<?php

namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Validator;
use Arr;
use DB;
use Datatables;
use Illuminate\Database\Eloquent\Collection;
use App\Setting;

class RestricalUsernameCtr extends Controller
{
	/**
	* Setting . IP Block Data
	* @return Response
	*/
	function getData(Request $request)
	{
		/* Load by ajax only */
		if(!$request->ajax()){return die('restrical.access');}
		
		$fetch = DB::table('setting')->where('setting_name','RESTRICAL_USERNAME')->first();
		$data = json_decode($fetch->setting_value);		
		$col = new Collection;

		foreach ($data as $key => $value) {
	      $col->push([
	        'ip'=>'<div>'.$value.'</div>',
			'chkbox'=>'<input type="checkbox" name="deleteItems[]" value="'.$value.'" />'
	      ]);
	    }

	    return Datatables::of($col)
	    ->rawColumns(['ip','chkbox'])
	    ->make(true);
	}
	
	/**
	* Setting . IP Block
	* @return Response
	*/
	function index(Request $request)
	{
		if(!Setting::where('setting_name','RESTRICAL_USERNAME')->first()){
			Setting::create(['setting_name'=>'RESTRICAL_USERNAME','setting_value'=>'[]']);
		}
		return view('backend.restrical_username');
	}
	
	/**
	* Setting . Create IP Block
	* @return Response
	*/
	function getCreate(Request $request)
	{		
		return view('backend.restrical_username_create');
	}
	
	/**
	* Setting . IP Block . Post
	* @return Response
	*/
	function postCreate(Request $request)
	{
		/* Validate */
		$validator = Validator::make($request->all(), [
			'username' => 'required',
		]);
		
		/* Ajax Response Validate */
		if($request->ajax()){
			if (!$validator->passes()) {
				//return response('Unauthorized.', 401);
				return response()->json(['error'=>$validator->errors()->all()]);
			}
		}
		
		/* Normal POST */
		$fetch = DB::table('setting')->where('setting_name','RESTRICAL_USERNAME')->first();
		$data = json_decode($fetch->setting_value);
		if(!in_array($request->username, $data)){
			$data = json_encode(Arr::prepend($data, $request->username));
			DB::table('setting')->where('setting_name','RESTRICAL_USERNAME')->update(['setting_value'=>$data]);
		}
		// $setting = DB::table('setting')->where('setting_name','RESTRICAL_USERNAME')->first();
		
		if($request->ajax()){
			return response()->json(['message'=>[trans('message.save.success')]]);
		}
		return redirect()->back()->with('msg',trans('message.save.success'));
		
	}
	
	/**
	* Setting . IP Block . Delete
	* @return Response
	*/
	function postDelete(Request $request)
	{
		/* Validate */
		$validator = Validator::make($request->all(), [
			'deleteItems' => 'required',
		]);
		
		/* Ajax Response Validate */
		if (!$validator->passes()) {
			if($request->ajax()){
				//return response('Unauthorized.', 401);
				return response()->json(['error'=>$validator->errors()->all()]);
			}
		}
		
		/* Normal POST */
		$fetch = DB::table('setting')->where('setting_name','RESTRICAL_USERNAME')->first();
		$data = json_decode($fetch->setting_value,true);
		
		/* Find & unset array value */
		$datax = array_diff($data, $request->deleteItems);		
		$data = Arr::flatten($datax);
		$data = json_encode($data);
		DB::table('setting')->where('setting_name','RESTRICAL_USERNAME')->update(['setting_value'=>$data]);
	
		
		if($request->ajax()){
			return response()->json(['message'=>[trans('message.delete.success')]]);
		}
		return redirect()->back()->with('msg',trans('message.update.success'));	
	}
	
	
	
}
