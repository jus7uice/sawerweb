<?php

namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Curl;
use Validator;
use DB;
use Datatables;
use Illuminate\Database\Eloquent\Collection;
use App\Setting;

class SettingPgCtr extends Controller
{
	/**
	* Setting
	* @return Response
	*/
	function index(Request $request)
	{
								
		if(!DB::table('setting')->where('setting_name','PG_API_URL')->first()){
			DB::table('setting')->insert(['setting_name'=>'PG_API_URL','setting_value'=>'']);
		}
		if(!DB::table('setting')->where('setting_name','PG_API_KEY')->first()){
			DB::table('setting')->insert(['setting_name'=>'PG_API_KEY','setting_value'=>'']);
		}
		if(!DB::table('setting')->where('setting_name','PG_API_SECRET')->first()){
			DB::table('setting')->insert(['setting_name'=>'PG_API_SECRET','setting_value'=>'']);
		}
		if(!DB::table('setting')->where('setting_name','PG_CLIENT_ID')->first()){
			DB::table('setting')->insert(['setting_name'=>'PG_CLIENT_ID','setting_value'=>'']);
		}
		if(!DB::table('setting')->where('setting_name','PG_CLIENT_SECRET')->first()){
			DB::table('setting')->insert(['setting_name'=>'PG_CLIENT_SECRET','setting_value'=>'']);
		}
		if(!DB::table('setting')->where('setting_name','PG_API_TOKEN')->first()){
			DB::table('setting')->insert(['setting_name'=>'PG_API_TOKEN','setting_value'=>'']);
		}
		
		$data['pg_api_url'] = Setting::where('setting_name','PG_API_URL')->pluck('setting_value')->first();
		$data['pg_api_key'] = Setting::where('setting_name','PG_API_KEY')->pluck('setting_value')->first();
		$data['pg_api_secret'] = Setting::where('setting_name','PG_API_SECRET')->pluck('setting_value')->first();
		$data['pg_client_id'] = Setting::where('setting_name','PG_CLIENT_ID')->pluck('setting_value')->first();
		$data['pg_client_secret'] = Setting::where('setting_name','PG_CLIENT_SECRET')->pluck('setting_value')->first();
		$data['pg_api_token'] = Setting::where('setting_name','PG_API_TOKEN')->pluck('setting_value')->first();
		$data['pg_api_token_expired_at'] = Setting::where('setting_name','PG_API_TOKEN')->pluck('updated_at')->first();
		
		return view('backend.setting_pg',compact('data'));		
	}

	function postUpdate(Request $request)
	{
		/* Validate */
		$request->validate([
			'pg_api_url' => 'required',
			'pg_api_key' => 'required',
			'pg_api_secret' => 'required',
			'pg_client_id' => 'required',
			'pg_client_secret' => 'required',
		]);
	
	
		debug($request->all());
		
		/* Save to DB */
		DB::table('setting')->where('setting_name','PG_API_URL')->update(['setting_value' => $request->pg_api_url]);
		DB::table('setting')->where('setting_name','PG_API_KEY')->update(['setting_value' => $request->pg_api_key]);
		DB::table('setting')->where('setting_name','PG_API_SECRET')->update(['setting_value' => $request->pg_api_secret]);
		DB::table('setting')->where('setting_name','PG_CLIENT_ID')->update(['setting_value' => $request->pg_client_id]);
		DB::table('setting')->where('setting_name','PG_CLIENT_SECRET')->update(['setting_value' => $request->pg_client_secret]);
		
		/* Redirect */
		return redirect()->back()->with('msg',trans('message.update.success'));
	}
	
	
	function getToken(Request $request)
	{
		$pg_api_url = Setting::where('setting_name','PG_API_URL')->pluck('setting_value')->first();
		$pg_client_id = Setting::where('setting_name','PG_CLIENT_ID')->pluck('setting_value')->first();
		$pg_client_secret = Setting::where('setting_name','PG_CLIENT_SECRET')->pluck('setting_value')->first();
				
		$data = [
			'grant_type' => 'client_credentials',
			'client_id'  => $pg_client_id,
			'client_secret' => $pg_client_secret,
		];

		/* 1 GET Access. Let log user in ( one time only for all next requests ) */
		$response = Curl::to($pg_api_url.'api/oauth/token')
		->withData($data)
		->asJson()
		->post();
		
		if(is_object($response)){
			if(isset($response->token_type)){
			
				// debug('new call sucess');
				$type = $response->token_type;
				$token = $response->access_token;					
				$expires_in = $response->expires_in;					
				// $next_date = date("Y-m-d H:i:s",strtotime("+5 day", time()));					
				$next_date = date("Y-m-d H:i:s",(time() + $expires_in));					
				$pg_token = $type.' '.$token;
				DB::table('setting')->where('setting_name','PG_API_TOKEN')->update([
					'setting_value' => $pg_token,
					'updated_at' => $next_date,
				]);
			} else {
				debug($response);
				exit();
			}
		}
		
		
		/* Rediret Success */
		if($request->ajax()){
			return response()->json(['message'=>[trans('message.update.success')]]);
		}
		return redirect()->back()->with('msg',trans('message.update.success'));
	}
	
}
