<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB;
use Validator;
use Illuminate\Validation\Rule;
use Datatables;

use App\CoinsPackage;

class CoinsPackageCtr extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		return view('backend.coins_package');
    }
	
	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getData(Request $request)
    {		
		$rows = CoinsPackage::where('status','<',2);
        return Datatables::of($rows)
		->addColumn('chkbox',function($row){
			return '<input type="checkbox" name="deleteItems[]" value="'.$row->id.'" />';
		})
		->addColumn('status',function($row){
			if($row->status == 1)	return '<span class="label label-success">Active</span>';
			else return '<span class="label label-danger">Not Active</span>';
		})
		->addColumn('action',function($row){
			$action = '
				<a href="'.url(ADMIN_PATH.'coins.package.edit?id='.$row->id).'" data-toggle="ajaxModal">'.trans('general.button.edit').'</a>
			';
			return $action;
		})
		->addColumn('lbl_created_at',function($row){
			return date("Y-m-d H:i:s",strtotime($row->created_at));
		})
		->rawColumns(['chkbox','lbl_post_tag','lbl_created_at','status','action'])
		->make(true);
    }
	
	
	/**
     * Create a resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCreate(Request $request)
    {
		return view('backend.coins_package_create');
    }
	
	/**
     * Create a resource : POST.
     *
     * @return \Illuminate\Http\Response
     */
    public function postCreate(Request $request)
    {		
		/* Validate */
		$validator = Validator::make($request->all(), [
			'title' => 'required|string',
			'price' => 'required|numeric',
			'total_coins' => 'required|numeric',
		]);
		
		/* Ajax Response Validate */
		if($request->ajax()){
			if (!$validator->passes()) {
				return response()->json(['error'=>$validator->errors()->all()]);
			}
		}
		
		
		/* Save to DB */
		$row = new CoinsPackage;
		$row->fill($request->all());
		$row->save();
		
		/* Redirect */
		return redirect()->back()->with('msg',trans('message.save.success'));
    }
	
	/**
     * Edit a resource.
     *
     * @return \Illuminate\Http\Response
     */
   public function getEdit(Request $request)
    {
		$data = CoinsPackage::find($request->id);
		return view('backend.coins_package_edit',compact('data'));
    }
	
	
	/**
     * Edit a resource : POST.
     *
     * @return \Illuminate\Http\Response
     */
    public function postEdit(Request $request)
    {
		/* Validate */
		$validator = Validator::make($request->all(), [
			'title' => 'required|string',
			'price' => 'required|numeric',
			'total_coins' => 'required|numeric',
		]);
		
		/* Ajax Response Validate */
		if($request->ajax()){
			if (!$validator->passes()) {
				return response()->json(['error'=>$validator->errors()->all()]);
			}
		}
		
		// die(debug($request->all()));
				
		// dd($request->all());
		/* Save to DB */
		$row = CoinsPackage::find($request->id);
		$row->fill($request->all());
		$row->save();
	
		/* Redirect */
		return redirect()->back()->with('msg',trans('message.save.success'));
    }
	
	
	 /**
     * Delete resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function postDelete(Request $request)
    {		
		/* Validate */
		$validator = Validator::make($request->all(), [
			'deleteItems' => 'required',
		]);
		
		/* Ajax Response Validate */
		if($request->ajax()){
			if (!$validator->passes()) {
				//return response('Unauthorized.', 401);
				return response()->json(['error'=>$validator->errors()->all()]);
			}
		}
		
		/* If group exist */
		DB::table('topup_packages')->whereIn('id',$request->deleteItems)->update(['deleted_at'=>date("Y-m-d H;i:s"),'status'=>3]);
		
		/* Response */
		if($request->ajax()){
			return response()->json(['message'=>[trans('message.delete.success')]]);
		}
		return redirect()->back()->with('msg',trans('message.delete.success'));	
    }
}
