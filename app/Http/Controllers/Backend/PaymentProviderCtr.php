<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB;
use Storage;
use Validator;
use Illuminate\Validation\Rule;
use Datatables;

use App\PaymentProvider;

class PaymentProviderCtr extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		return view('backend.payment_provider');
    }
	
	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getData(Request $request)
    {		
		$rows = PaymentProvider::where('status','<',2);
        return Datatables::of($rows)
		->addColumn('chkbox',function($row){
			return '<input type="checkbox" name="deleteItems[]" value="'.$row->id.'" />';
		})
		->addColumn('status',function($row){
			if($row->status == 1)	return '<span class="label label-success">Active</span>';
			else return '<span class="label label-danger">Not Active</span>';
		})
		->addColumn('action',function($row){
			$action = '
				<a href="'.url(ADMIN_PATH.'payment.provider.edit?id='.$row->id).'" data-toggle="ajaxModal">'.trans('general.button.edit').'</a> &nbsp;|&nbsp; 
				<a href="'.url(ADMIN_PATH.'payment.provider.gettoken?id='.$row->id).'">GET: Token</a>
			';
			return $action;
		})
		->addColumn('lbl_created_at',function($row){
			return date("Y-m-d H:i:s",strtotime($row->created_at));
		})
		->rawColumns(['chkbox','lbl_post_tag','lbl_created_at','status','action'])
		->make(true);
    }
	
	
	/**
     * Create a resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCreate(Request $request)
    {
		/* Module Payment List */
		$paymodules = Storage::disk('paylib')->files();
		$payFiles = array();		
		foreach($paymodules as $mod){
			$payFiles[str_replace('.php','',$mod)] = str_replace('.php','',$mod);
		}
		// debug($payFiles);
		
		return view('backend.payment_provider_create',compact('payFiles'));
    }
	
	/**
     * Create a resource : POST.
     *
     * @return \Illuminate\Http\Response
     */
    public function postCreate(Request $request)
    {		
		/* Validate */
		$validator = Validator::make($request->all(), [
			'name' => 'required|string',
			'api_url' => 'required|url',
		]);
		
		/* Ajax Response Validate */
		if($request->ajax()){
			if (!$validator->passes()) {
				return response()->json(['error'=>$validator->errors()->all()]);
			}
		}
		
		
		/* Save to DB */
		$row = new PaymentProvider;
		$row->fill($request->all());
		$row->save();
		
		/* Redirect */
		return redirect()->back()->with('msg',trans('message.save.success'));
    }
	
	/**
     * Edit a resource.
     *
     * @return \Illuminate\Http\Response
     */
	public function getEdit(Request $request)
    {
		$data = PaymentProvider::find($request->id); //debug($data);
		
		/* Module Payment List */
		$paymodules = Storage::disk('paylib')->files();
		$payFiles = array();		
		foreach($paymodules as $mod){
			$payFiles[str_replace('.php','',$mod)] = str_replace('.php','',$mod);
		}
		// debug($payFiles);
		
		return view('backend.payment_provider_edit',compact('data','payFiles'));
    }
	
	
	/**
     * Edit a resource : POST.
     *
     * @return \Illuminate\Http\Response
     */
    public function postEdit(Request $request)
    {
		/* Validate */
		$validator = Validator::make($request->all(), [
			'name' => 'required|string',
			'api_url' => 'required|url',
		]);
		
		/* Ajax Response Validate */
		if($request->ajax()){
			if (!$validator->passes()) {
				return response()->json(['error'=>$validator->errors()->all()]);
			}
		}
		
		// die(debug($request->all()));
				
		// dd($request->all());
		/* Save to DB */
		$row = PaymentProvider::find($request->id);
		$row->fill($request->all());
		$row->save();
		
		/* Redirect */
		return redirect()->back()->with('msg',trans('message.save.success'));
    }
	
	
	 /**
     * Delete resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function postDelete(Request $request)
    {		
		/* Validate */
		$validator = Validator::make($request->all(), [
			'deleteItems' => 'required',
		]);
		
		/* Ajax Response Validate */
		if($request->ajax()){
			if (!$validator->passes()) {
				//return response('Unauthorized.', 401);
				return response()->json(['error'=>$validator->errors()->all()]);
			}
		}
		
		/* If group exist */
		DB::table('payment_providers')->whereIn('id',$request->deleteItems)->update(['deleted_at'=>date("Y-m-d H;i:s"),'status'=>3]);
		
		/* Response */
		if($request->ajax()){
			return response()->json(['message'=>[trans('message.delete.success')]]);
		}
		return redirect()->back()->with('msg',trans('message.delete.success'));	
    }
	
	
	/**
     * Get a resource.
     *
     * @return \Illuminate\Http\Response
     */
	public function getGetToken(Request $request)
    {
		if($data = PaymentProvider::find($request->id))
		{
			return view('backend.payment_provider_gettoken',compact("data"));
		}
		
		return redirect()->back()->with('msgError','Data not found');	
    }
	
	
	
	
	/**
     * Post a resource.
     *
     * @return \Illuminate\Http\Response
     */
	public function postGetToken(Request $request)
    {
		if($data = PaymentProvider::find($request->id))
		{
		
			/* Load Module */
			/* Upgrade tabel mutasi_$date */
			if(storage::disk('paylib')->exists($data->module_name.'.php')){
				$modul = '\App\Paylibs\\'.$data->module_name;
				$pg = new $modul; //// <--- this thing will be autoloaded

				/* Pastikan function Ada */
				if(method_exists($pg,'getToken'))
				{
					/* Call function */
					
					$response = $pg->getToken();
					
					if(is_object($response)){
						if(isset($response->token_type)){
						
							// debug('new call sucess');
							$type = $response->token_type;
							$token = $response->access_token;					
							$expires_in = $response->expires_in;					
							// $next_date = date("Y-m-d H:i:s",strtotime("+5 day", time()));					
							$next_date = date("Y-m-d H:i:s",(time() + $expires_in));					
							$pg_token = $type.' '.$token;
							PaymentProvider::where('id',$request->id)->update([
								'token' => $pg_token,
								'token_expired_at' => $next_date,
							]);
							
							return redirect()->back()->with('msg',trans('message.update.success'));
						}
					}
				}			
			}
		}
		
		return redirect()->back()->with('msgError','Data not found');	
    }
	
	
	
	
	
	
	
	
}
