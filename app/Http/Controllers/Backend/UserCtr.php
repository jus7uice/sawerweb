<?php

namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Hash;
use DB;
use Str;
use Validator;
use Illuminate\Validation\Rule;
use Datatables;
use Jenssegers\Agent\Agent;

use App\User;
use App\Organizer;

class UserCtr extends Controller
{
   	 /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		return view('backend.user');
    }

	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getData(Request $request)
    {		
		$rows = User::with('organizer')->where('users.status','<',2)->select('*');
        return Datatables::of($rows)
		->addColumn('chkbox',function($row){
			return '<input type="checkbox" name="deleteItems[]" value="'.$row->id.'" />';
		})
		->addColumn('lbl_created_at',function($row){
			return date("Y-m-d H:i:s",strtotime($row->created_at));
		})
		->addColumn('lbl_status',function($row){
			if($row->status == 1)	return '<span class="label label-success">Active</span>';
			else return '<span class="label label-danger">Not Active</span>';
		})
		->addColumn('action',function($row){
			$action = '
				<a href="'.url(ADMIN_PATH.'user.edit?id='.$row->id).'" data-toggle="ajaxModal">'.trans('general.button.edit').'</a>
			
			';
			return $action;
		})
		->addIndexColumn()
		->rawColumns(['chkbox','lbl_status','lbl_created_at','action'])
		->make();
    }
	
	
	/**
     * Create a resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCreate(Request $request)
    {
		/* Load by ajax only */
		if(!$request->ajax()){return redirect('err/restrical.access');}
		
		$organizer = Organizer::where('status','<',2)->pluck('title','id')->toArray();
		$organizerList = ['0'=>'Select Organizer :'] + $organizer;
		
		return view('backend.user_create',compact('organizerList'));
    }
	
	/**
     * Create a resource : POST.
     *
     * @return \Illuminate\Http\Response
     */
    public function postCreate(Request $request)
    {
		/* Call Agent */
		$agent = new Agent();	
		
		/* Validate */
		$validator = Validator::make($request->all(), [
			// 'organizer_id' => 'required',
			'name' => 'required',
			'email' => 'required|email|unique:users,email',
			// 'nickname' => 'required|unique:users,nickname',
			'password' => 'required|min:5',
			
		]);
		
		/* Ajax Response Validate */
		if($request->ajax()){
			if (!$validator->passes()) {
				//return response('Unauthorized.', 401);
				return response()->json(['error'=>$validator->errors()->all()]);
			}
		}
		// dd($request->all());
		$uid = date("dm").make_randnum(13);
		/* Save to DB */
		$row = new User;
		$row->fill($request->all());
		$row->uid = $uid;
		$row->username = $request->email;
		$row->nickname = create_slug(strtolower($request->name));
		$row->password = Hash::make($request->password);
		
		$row->ip = $request->ip();
		$row->http_headers = json_encode($request->header());
		$row->ua_flatform = $agent->platform();
		$row->ua_browser = $agent->browser();
		$row->remember_token = Str::random();
		$row->email_verified_at = now();
		
		$row->save();
				
		// /* Redirc */
		if($request->ajax()){
			return response()->json(['message'=>[trans('message.save.success')]]);
		}
		return redirect()->back()->with('msg',trans('message.save.success'));
    }
	
	/**
     * Edit a resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getEdit(Request $request)
    {
		/* Load by ajax only */
		if(!$request->ajax()){return redirect('err/restrical.access');}
		
		$organizer = Organizer::where('status','<',2)->pluck('title','id')->toArray();
		$organizerList = ['0'=>'Select Organizer :'] + $organizer;
		
		$data = User::find($request->id);		
		return view('backend.user_edit',compact('data','organizerList'));
    }
	
	/**
     * Edit a resource : POST.
     *
     * @return \Illuminate\Http\Response
     */
    public function postEdit(Request $request)
    {
		/* Validate */
		$validator = Validator::make($request->all(), [
			'email' => [
				'required',
				Rule::unique('users')->ignore($request->id),
			],
			// 'nickname' => 'required|min:6',
			// 'nickname' => [
				// 'required',
				// Rule::unique('users')->ignore($request->id),
			// ],
			'name' => 'required',
		]);

		if($request->has('password') && strlen($request->password) > 0 && strlen($request->password) < 3){
			return response()->json(['error'=>[trans('validation.min.string',['attribute'=>'Password','min'=>'3'])]]);
		}
		
		/* Ajax Response Validate */
		if($request->ajax()){
			if (!$validator->passes()) {
				//return response('Unauthorized.', 401);
				return response()->json(['error'=>$validator->errors()->all()]);
			}
		}
		// dd($request->all());
		/* Save to DB */
		$row = User::find($request->id);
		// $row->fill($request->all());
		if($request->has('password') && strlen($request->password)>2){
			$row->password = Hash::make($request->password);
		}		
		$row->organizer_id = $request->organizer_id;
		$row->is_creator = $request->is_creator;
		$row->name = $request->name;
		$row->nickname = $request->nickname;
		$row->email = $request->email;
		$row->remember_token = Str::random();
		$row->email_verified_at = $request->email_verified_at;
		$row->status = $request->status;
		$row->save();
				
		// /* Redirc */
		if($request->ajax()){
			return response()->json(['message'=>[trans('message.save.success')]]);
		}
		return redirect()->back()->with('msg',trans('message.save.success'));
    }
	
	 /**
     * Delete resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function postDelete(Request $request)
    {		
		/* Validate */
		$validator = Validator::make($request->all(), [
			'deleteItems' => 'required',
		]);
		
		/* Ajax Response Validate */
		if($request->ajax()){
			if (!$validator->passes()) {
				//return response('Unauthorized.', 401);
				return response()->json(['error'=>$validator->errors()->all()]);
			}
		}
		
		/* If group exist */
		DB::table('users')->whereIn('id',$request->deleteItems)->delete();
		
		/* Response */
		if($request->ajax()){
			return response()->json(['message'=>[trans('message.delete.success')]]);
		}
		return redirect()->back()->with('msg',trans('message.delete.success'));	
    }
}
