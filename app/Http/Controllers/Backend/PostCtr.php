<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB;
use Validator;
use Illuminate\Validation\Rule;
use Datatables;
use LaravelVideoEmbed;
use Jenssegers\Agent\Agent;


use App\Post;
use App\Category;
use App\PostTag;

class PostCtr extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		return view('backend.post');
    }
	
	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getData(Request $request)
    {		
		$rows = Post::with('category','user')->where('posts.status','<',2)->select('posts.*');
        return Datatables::of($rows)
		->addColumn('chkbox',function($row){
			return '<input type="checkbox" name="deleteItems[]" value="'.$row->id.'" />';
		})
		->addColumn('lbl_post_tag',function($row){
			$fetch = DB::table('post_tags')->leftJoin('tags','tags.id','=','post_tags.tag_id')->where('post_tags.post_id',$row->id)->pluck('tags.name')->toArray();
			return implode(', ',$fetch);
		})
		->addColumn('status',function($row){
			if($row->status == 1)	return '<span class="label label-success">Active</span>';
			else return '<span class="label label-danger">Not Active</span>';
		})
		->addColumn('action',function($row){
			$action = '
				<a href="'.url(ADMIN_PATH.'post.edit?id='.$row->id).'">'.trans('general.button.edit').'</a>
			
			';
			return $action;
		})
		->addColumn('lbl_created_at',function($row){
			return date("Y-m-d H:i:s",strtotime($row->created_at));
		})
		->rawColumns(['chkbox','lbl_post_tag','lbl_created_at','status','action'])
		->make(true);
    }
	
	
	/**
     * Create a resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCreate(Request $request)
    {
		
		//URL to be used for embed generation
		$url = "https://www.youtube.com/watch?v=erYVavLimAM";

		//Optional array of website names, if present any websites not in the array will result in false being returned by the parser
		$whitelist = ['YouTube', 'Vimeo'];

		//Optional parameters to be appended to embed
		$params = [
			'autoplay' => 1,
			'loop' => 1
		  ];

		//Optional attributes for embed container
		$attributes = [
		  'type' => null,
		  'class' => 'iframe-class',
		  'data-html5-parameter' => true,
		  'width' => 600,
		  'height' => 'auto',
		  'style' => 'border: 1px solid red;',
		];

		// return LaravelVideoEmbed::parse($url, $whitelist, $params, $attributes);

		
		$category = Category::where('status','<',2)->pluck('name','id')->toArray();
		$categoryList = [' '=>'Select Category :'] + $category;
		return view('backend.post_create',compact('categoryList'));
    }
	
	/**
     * Create a resource : POST.
     *
     * @return \Illuminate\Http\Response
     */
    public function postCreate(Request $request)
    {		
		/* Validate */
		$request->validate([
			'title' => 'required|unique:posts,title',
			'category_id' => 'required',
			'video_url' => 'required',
			'description' => 'required',
		]);		
		
		/* Call Agent */
		$agent = new Agent();		
		// debug($request->all());
		
		// debug($agent);
		// debug($request->header());
		
		// debug($request->server('HTTP_USER_AGENT'));
		
		// $browser = $agent->browser();
		// debug($browser);
		
		
		/* Save to DB */
		$row = new Post;
		$row->fill($request->all());
		$row->hashed_id = md5($request->video_url . time());
		$row->description = str_encode($request->description);
		
		$row->http_headers = json_encode($request->header());
		$row->ua_flatform = $agent->platform();
		$row->ua_browser = $agent->browser();
		$row->ip = $request->ip();		
		$row->save();
		
		// if(is_array($request->tag) && count($request->tag) > 0){
			// $data = [];
			// foreach($request->tag as $tag){
				// $data[] = ['post_id'=>$row->id,'tag_id'=>$tag];
			// }
			// PostTag::insert($data);
		// }
				
		// /* Redirc */
		// return redirect()->back()->with('msg',trans('message.save.success'));
    }
	
	/**
     * Edit a resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getEdit(Request $request)
    {
		$request->validate([
			'id' => 'required',
		]);
		
		$data = Post::find($request->id);
		$category = Category::where('status','<',2)->pluck('name','id')->toArray();
		$categoryList = [' '=>'Select Category :'] + $category;
		
		$postTag = PostTag::with('tag')->where('post_tags.post_id',$request->id)->get();
		$PostTagList = [];
		foreach($postTag as $row){
			$PostTagList = $PostTagList + [$row->tag_id => $row->tag->name];
		}
		
		// die(debug($PostTagList));
		return view('backend.post_edit',compact('data','categoryList','PostTagList'));
    }
	
	
	/**
     * Edit a resource : POST.
     *
     * @return \Illuminate\Http\Response
     */
    public function postEdit(Request $request)
    {
		/* Validate */
		// $validator = Validator::make($request->all(), [
		$request->validate([
			'title' => [
				'required',
				Rule::unique('posts')->ignore($request->id),
			],
			'video_url' => [
				'required',
				Rule::unique('posts')->ignore($request->id),
			],
			'category_id' => 'required',
			'description' => 'required',
		]);
				
		// dd($request->all());
		/* Save to DB */
		$row = Post::find($request->id);
		$row->fill($request->all());
		$row->description = str_encode($request->description);
		$row->save();
				
		if(is_array($request->tag) && count($request->tag) > 0){
			DB::table('post_tags')->where('post_id',$request->id)->delete();
			$data = [];
			foreach($request->tag as $tag){
				$data[] = ['post_id'=>$row->id,'tag_id'=>$tag];
			}
			PostTag::insert($data);
		}
		
		// /* Redirc */
		/* Redirc */
		return redirect()->back()->with('msg',trans('message.save.success'));
    }
	
}
