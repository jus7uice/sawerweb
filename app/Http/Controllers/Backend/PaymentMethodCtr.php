<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB;
use Validator;
use Illuminate\Validation\Rule;
use Datatables;

use App\PaymentProvider;
use App\PaymentMethod;

class PaymentMethodCtr extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		return view('backend.payment_method');
    }
	
	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getData(Request $request)
    {		
		$rows = PaymentMethod::with('provider');
        return Datatables::of($rows)
		->addColumn('chkbox',function($row){
			return '<input type="checkbox" name="deleteItems[]" value="'.$row->id.'" />';
		})
		->addColumn('status',function($row){
			if($row->status == 1)	return '<span class="label label-success">Active</span>';
			else return '<span class="label label-danger">Not Active</span>';
		})
		->addColumn('action',function($row){
			$action = '
				<a href="'.url(ADMIN_PATH.'payment.provider.edit?id='.$row->id).'" data-toggle="ajaxModal">'.trans('general.button.edit').'</a>
			';
			return $action;
		})
		->addColumn('lbl_created_at',function($row){
			return date("Y-m-d H:i:s",strtotime($row->created_at));
		})
		->rawColumns(['chkbox','lbl_post_tag','lbl_created_at','status','action'])
		->make(true);
    }
	
	
	/**
     * Create a resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCreate(Request $request)
    {
		$provider = PaymentProvider::where('status','<',2)->pluck('name','id')->toArray();
		$providerList = [' '=>'Select Provider :'] + $provider;
		return view('backend.payment_method_create',compact('providerList'));
    }
	
	/**
     * Create a resource : POST.
     *
     * @return \Illuminate\Http\Response
     */
    public function postCreate(Request $request)
    {		
		/* Validate */
		$validator = Validator::make($request->all(), [
			'name' => 'required|string',
			'code' => 'required|string',
		]);
		
		/* Ajax Response Validate */
		if($request->ajax()){
			if (!$validator->passes()) {
				return response()->json(['error'=>$validator->errors()->all()]);
			}
		}
		
		
		/* Save to DB */
		$row = new PaymentMethod;
		$row->fill($request->all());
		$row->save();
		
		/* Redirect */
		return redirect()->back()->with('msg',trans('message.save.success'));
    }
	
	/**
     * Edit a resource.
     *
     * @return \Illuminate\Http\Response
     */
   public function getEdit(Request $request)
    {
		$data = PaymentMethod::find($request->id); //debug($data);
		return view('backend.payment_method_edit',compact('data'));
    }
	
	
	/**
     * Edit a resource : POST.
     *
     * @return \Illuminate\Http\Response
     */
    public function postEdit(Request $request)
    {
		/* Validate */
		$validator = Validator::make($request->all(), [
			'name' => 'required|string',
			'api_url' => 'required|url',
		]);
		
		/* Ajax Response Validate */
		if($request->ajax()){
			if (!$validator->passes()) {
				return response()->json(['error'=>$validator->errors()->all()]);
			}
		}
		
		// die(debug($request->all()));
				
		// dd($request->all());
		/* Save to DB */
		$row = PaymentMethod::find($request->id);
		$row->fill($request->all());
		$row->save();
	
		/* Redirect */
		return redirect()->back()->with('msg',trans('message.save.success'));
    }
	
	
	 /**
     * Delete resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function postDelete(Request $request)
    {		
		/* Validate */
		$validator = Validator::make($request->all(), [
			'deleteItems' => 'required',
		]);
		
		/* Ajax Response Validate */
		if($request->ajax()){
			if (!$validator->passes()) {
				//return response('Unauthorized.', 401);
				return response()->json(['error'=>$validator->errors()->all()]);
			}
		}
		
		/* If group exist */
		DB::table('payment_methods')->whereIn('id',$request->deleteItems)->update(['deleted_at'=>date("Y-m-d H;i:s"),'status'=>3]);
		
		/* Response */
		if($request->ajax()){
			return response()->json(['message'=>[trans('message.delete.success')]]);
		}
		return redirect()->back()->with('msg',trans('message.delete.success'));	
    }
}
