<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB;
use Validator;
use Illuminate\Validation\Rule;
use Datatables;
use Image;

use App\StickerStock;

class StickerStockCtr extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		return view('backend.sticker_stock');
    }
	
	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getData(Request $request)
    {		
		$rows = StickerStock::where('status','<',2);
        return Datatables::of($rows)
		->addColumn('chkbox',function($row){
			return '<input type="checkbox" name="deleteItems[]" value="'.$row->id.'" />';
		})
		->addColumn('status',function($row){
			if($row->status == 1)	return '<span class="label label-success">Active</span>';
			else return '<span class="label label-danger">Not Active</span>';
		})
		->addColumn('img',function($row){
			if($row->media_url == "")	return '<em>No Media</em>';
			else return '<a href="'.url($row->media_url).'" class="fancybox"><img src="'.url($row->media_url).'" height="50" /></a>';
		})
		->addColumn('action',function($row){
			$action = '
				<a href="'.url(ADMIN_PATH.'sticker.stock.edit?id='.$row->id).'" data-toggle="ajaxModal">'.trans('general.button.edit').'</a>
			';
			return $action;
		})
		->addColumn('lbl_created_at',function($row){
			return date("Y-m-d H:i:s",strtotime($row->created_at));
		})
		->rawColumns(['chkbox','img','lbl_created_at','status','action'])
		->make(true);
    }
	
	
	/**
     * Create a resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCreate(Request $request)
    {
		return view('backend.sticker_stock_create');
    }
	
	/**
     * Create a resource : POST.
     *
     * @return \Illuminate\Http\Response
     */
    public function postCreate(Request $request)
    {		
		/* Validate */
		$validator = Validator::make($request->all(), [
			'title' => 'required|string',
			'price' => 'required|numeric',
			'file' => 'required',
		]);
		
		/* Ajax Response Validate */
		if($request->ajax()){
			if (!$validator->passes()) {
				return response()->json(['error'=>$validator->errors()->all()]);
			}
		}
		
		// files storage folder
		$dir = UPLOAD_PATH.'sticker';	
		
		
		if ($request->hasFile('file'))
		{
			/* MKDIR */
			if (!file_exists($dir)) {mkdir($dir, 0777, true);}
			/* SET DIR */
			$dir = $dir.'/';
									
			/* START UPLOAD */
			$path = $request->file->path();
			$extension = strtolower($request->file->getClientOriginalExtension());
			$filename = strtolower(create_slug($request->title).'.'.$extension);
			$fileupload = $dir.$filename;
			if ($extension == 'png'|| $extension == 'jpg' || $extension == 'bmp' || $extension == 'gif' || $extension == 'jpeg'|| $extension == 'pjpeg')
			{					
				
				$upload = Image::make($path);
				$upload->save($fileupload);
		
				DB::table('sticker_stocks')
				->insert([
					'title' => $request->title,
					'media_url' => $fileupload,
					'price' => $request->price,
					'created_at' => date('Y-m-d H:i:s'),
					'updated_at' => date('Y-m-d H:i:s'),
				]);				
				
				return redirect()->back()->with('msg',trans('message.save.success'));
			}
		}
		
		/* Save to DB */
		// $row = new StickerStock;
		// $row->fill($request->all());
		// $row->save();
		
		/* Redirect */
    }
	
	/**
     * Edit a resource.
     *
     * @return \Illuminate\Http\Response
     */
   public function getEdit(Request $request)
    {
		$data = StickerStock::find($request->id);
		return view('backend.sticker_stock_edit',compact('data'));
    }
	
	
	/**
     * Edit a resource : POST.
     *
     * @return \Illuminate\Http\Response
     */
    public function postEdit(Request $request)
    {
		/* Validate */
		$validator = Validator::make($request->all(), [
			'title' => [
				'required',
				Rule::unique('sticker_stocks')->ignore($request->id),'string'
			],
			'price' => 'required|numeric',
		]);
		
		/* Ajax Response Validate */
		if($request->ajax()){
			if (!$validator->passes()) {
				return response()->json(['error'=>$validator->errors()->all()]);
			}
		}
		
		/* Save to DB */
		$row = StickerStock::find($request->id);
		$row->fill($request->all());
		$row->save();
	
		/* Redirect */
		return redirect()->back()->with('msg',trans('message.save.success'));
    }
	
	
	 /**
     * Delete resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function postDelete(Request $request)
    {		
		/* Validate */
		$validator = Validator::make($request->all(), [
			'deleteItems' => 'required',
		]);
		
		/* Ajax Response Validate */
		if($request->ajax()){
			if (!$validator->passes()) {
				//return response('Unauthorized.', 401);
				return response()->json(['error'=>$validator->errors()->all()]);
			}
		}
		
		/* If group exist */
		DB::table('topup_packages')->whereIn('id',$request->deleteItems)->update(['deleted_at'=>date("Y-m-d H;i:s"),'status'=>3]);
		
		/* Response */
		if($request->ajax()){
			return response()->json(['message'=>[trans('message.delete.success')]]);
		}
		return redirect()->back()->with('msg',trans('message.delete.success'));	
    }
}
