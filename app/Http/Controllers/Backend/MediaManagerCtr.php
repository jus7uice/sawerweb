<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB;
use Validator;
use Illuminate\Validation\Rule;
use Datatables;
use Image;

use App\Media;

class MediaManagerCtr extends Controller
{
	 /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		$data = Media::where('type','IMAGE')->select('id','path', 'thumb', 'bigimg', 'title','created_at')->paginate(1);
		if ($request->ajax()) {			
			if($data)
			{
				return view('backend.media_list',['data'=>$data])->render();
			}			
		} 
		return view('backend.media',compact('data'));
    }
	
	/**
     * Create a resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCreate(Request $request)
    {
		/* Load by ajax only */
		if(!$request->ajax()){return redirect('err/restrical.access');}
		
		return view('backend.media_create');
    }

	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getData(Request $request)
    {		
		$rows = Media::where('type','IMAGE')->select('path', 'thumb', 'bigimg', 'title');
        return Datatables::of($rows)
		->addColumn('chkbox',function($row){
			return '<input type="checkbox" name="deleteItems[]" value="'.$row->id.'" />';
		})
		->addColumn('thumbnails',function($row){
			$thumb = $row->path.$row->thumb;
			$img = "";
			if(file_exists($thumb))
			{
				$img = '<img src="'.url($thumb).'" height="100" />';
			}			
			return $img;
		})		
		->addColumn('action',function($row){
			$action = '
				<a href="'.url(ADMIN_PATH.'channel.delete?id='.$row->id).'" data-toggle="ajaxModal">'.trans('general.button.delete').'</a>
			
			';
			return $action;
		})
		->rawColumns(['chkbox','thumbnails','action'])
		->make();
    }
	
	/**
     * Create a resource : POST.
     *
     * @return \Illuminate\Http\Response
     */
    public function postCreate(Request $request)
    {
		/* Validate */
		$validator = Validator::make($request->all(), [
			'title' => 'required',
			'file' => 'required',
		]);
		
		/* Ajax Response Validate */
		if (!$validator->passes()) {
			if($request->ajax()){
				//return response('Unauthorized.', 401);
				return response()->json(['error'=>$validator->errors()->all()]);
			}
			// debug($validator->errors()->all());
			return redirect()->back()->withErrors($validator->errors()->all());
		}
		
		// files storage folder
		$dir = UPLOAD_PATH.'media/'.date('Ymd');	
		
		if ($request->hasFile('file'))
		{
			/* MKDIR */
			if (!file_exists($dir)) {mkdir($dir, 0777, true);}
			/* SET DIR */
			$dir = $dir.'/';
									
			/* START UPLOAD */
			$path = $request->file->path();
			$extension = strtolower($request->file->getClientOriginalExtension());
			$filename = strtolower(create_slug(substr($request->file->getClientOriginalName(),0,-4)).'_'.time().'.'.$extension);
			$filename_thumb = strtolower(create_slug(substr($request->file->getClientOriginalName(),0,-4)).'_'.time().'_thumb.'.$extension);
			$fileupload = $dir.$filename;
			if ($extension == 'png'|| $extension == 'jpg' || $extension == 'bmp' || $extension == 'gif' || $extension == 'jpeg'|| $extension == 'pjpeg')
			{
					// debug('1. intervention');
					$big_img = Image::make($path);
					$big_img->widen(1000, function ($constraint) {$constraint->upsize();});				
					$big_img->save($dir.$filename, 90);	// save to local is success
								
					// image upload			
					$thumb_img = Image::make($path);
					$thumb_img->fit(400);
					$thumb_img->save($dir.$filename_thumb, 70);// save file
					
					DB::table('media')
					->insert([
						'type' => 'IMAGE',
						'title' => $request->title,
						'thumb' => $filename_thumb,
						'bigimg' => $filename,
						'path' => $dir,
						'ext' => $extension,
						'created_at' => date('Y-m-d H:i:s'),
						'updated_at' => date('Y-m-d H:i:s'),
					]);
				
			}
		}
		
		/* Redirc */
		if($request->ajax()){
			return response()->json(['message'=>[trans('message.save.success')]]);
		}
		return redirect()->back()->with('msg',trans('message.save.success'));
		
    }
	
	
	/**
     * Delete a resource : GET.
     *
     * @return \Illuminate\Http\Response
     */
    public function getDelete(Request $request)
    {
		/* Validate */
		$validator = Validator::make($request->all(), [
			'id' => 'required',
		]);
		
		/* Ajax Response Validate */
		if (!$validator->passes()) {
			if($request->ajax()){
				//return response('Unauthorized.', 401);
				return response()->json(['error'=>$validator->errors()->all()]);
			}
			// debug($validator->errors()->all());
			return redirect()->back()->withErrors($validator->errors()->all());
		}

		$data = Media::find($request->id);
		
		// files storage folder
		if(file_exists($data->path.$data->thumb)){
			debug("ADA THUMB");
			unlink($data->path.$data->thumb);
		}
		
		// files storage folder
		if(file_exists($data->path.$data->bigimg)){
			debug("ADA BIG IMG");
			unlink($data->path.$data->bigimg);
		}
		
		$data = Media::find($request->id)->delete();
		
		/* Redirc */
		if($request->ajax()){
			return response()->json(['message'=>[trans('message.delete.success')]]);
		}
		return redirect()->back()->with('msg',trans('message.delete.success'));
		
    }
}
