<?php

namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use DB;
use Validator;
use Illuminate\Validation\Rule;
use Datatables;

use App\Organizer;
use App\User;

class OrganizerCtr extends Controller
{
   	 /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		return view('backend.organizer');
    }

	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getData(Request $request)
    {		
		$rows = Organizer::with('user')->where('organizers.status','<',2)->select('organizers.*');
        return Datatables::of($rows)
		->addColumn('chkbox',function($row){
			return '<input type="checkbox" name="deleteItems[]" value="'.$row->id.'" />';
		})
		->addColumn('lbl_is_official',function($row){
			if($row->is_official > 0)	return '<span class="label label-success">Yes</span>';
			else return '<span class="label label-danger">No</span>';
		})
		->addColumn('status',function($row){
			if($row->status == 1)	return '<span class="label label-success">Active</span>';
			else return '<span class="label label-danger">Not Active</span>';
		})
		->addColumn('action',function($row){
			$action = '
				<a href="'.url(ADMIN_PATH.'organizer.edit?id='.$row->id).'" data-toggle="ajaxModal">'.trans('general.button.edit').'</a>
			
			';
			return $action;
		})
		->rawColumns(['chkbox','lbl_is_official','status','action'])
		->make();
    }
	
	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getJson(Request $request)
    {		
		if(!$request->ajax()){return die('restrical.access');}
		
		$data = Organizer::where('organizers.name','LIKE','%'.$request->q.'%')->select('name','id')->take(10)->get();		
		$json = [];
		foreach($data as $row){
			 $json[] = ['id'=>$row->id, 'text'=>$row->name];
		}
		return response()->json($json);
    }
	
	
	/**
     * Create a resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCreate(Request $request)
    {
		/* Load by ajax only */
		if(!$request->ajax()){return redirect('err/restrical.access');}
		
		return view('backend.organizer_create');
    }
	
	/**
     * Create a resource : POST.
     *
     * @return \Illuminate\Http\Response
     */
    public function postCreate(Request $request)
    {
		/* Validate */
		$validator = Validator::make($request->all(), [
			'title' => 'required|unique:organizers,title',
		]);
		
		/* Ajax Response Validate */
		if($request->ajax()){
			if (!$validator->passes()) {
				//return response('Unauthorized.', 401);
				return response()->json(['error'=>$validator->errors()->all()]);
			}
		}
		// dd($request->all());
		/* Save to DB */
		$row = new Organizer;
		$row->fill($request->all());
		$row->save();
				
		// /* Redirc */
		if($request->ajax()){
			return response()->json(['message'=>[trans('message.save.success')]]);
		}
		return redirect()->back()->with('msg',trans('message.save.success'));
    }
	
	/**
     * Edit a resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getEdit(Request $request)
    {
		/* Load by ajax only */
		if(!$request->ajax()){return redirect('err/restrical.access');}
		
		$data = Organizer::find($request->id);		
		
		$user = user::where('status','<',2)->pluck('name','id')->toArray();
		$userList = ['0'=>'Select User :'] + $user;
		
		return view('backend.organizer_edit',compact('data','userList'));
    }
	
	/**
     * Edit a resource : POST.
     *
     * @return \Illuminate\Http\Response
     */
    public function postEdit(Request $request)
    {
		/* Validate */
		$validator = Validator::make($request->all(), [
			'title' => [
				'required',
				Rule::unique('organizers')->ignore($request->id),
			],
		]);
		
		/* Ajax Response Validate */
		if($request->ajax()){
			if (!$validator->passes()) {
				//return response('Unauthorized.', 401);
				return response()->json(['error'=>$validator->errors()->all()]);
			}
		}
		// dd($request->all());
		/* Save to DB */
		$row = Organizer::find($request->id);
		$row->fill($request->all());
		$row->save();
				
		// /* Redirc */
		if($request->ajax()){
			return response()->json(['message'=>[trans('message.save.success')]]);
		}
		return redirect()->back()->with('msg',trans('message.save.success'));
    }
	
	 /**
     * Delete resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function postDelete(Request $request)
    {		
		/* Validate */
		$validator = Validator::make($request->all(), [
			'deleteItems' => 'required',
		]);
		
		/* Ajax Response Validate */
		if($request->ajax()){
			if (!$validator->passes()) {
				//return response('Unauthorized.', 401);
				return response()->json(['error'=>$validator->errors()->all()]);
			}
		}
		
		/* If group exist */
		DB::table('organizers')->whereIn('id',$request->deleteItems)->update(['status'=>3]);
		
		/* Response */
		if($request->ajax()){
			return response()->json(['message'=>[trans('message.delete.success')]]);
		}
		return redirect()->back()->with('msg',trans('message.delete.success'));	
    }
}
