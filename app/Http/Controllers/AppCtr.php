<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Storage;

use App\Post;
use App\User;
use App\Organizer;

class AppCtr extends Controller
{
	function index(Request $r){
		
		if (Auth::guard('member')->check())
		{
			$user = Auth::guard('member')->user();			
			// debug($user);
		}
		
		$video = Post::with('user')->where(['publish_status'=>1,'status'=>1])->orderBy('updated_at')->take(10)->get();		
		return View("home",compact("video"));
	}
	
	function getLogin(Request $request){		
		if (Auth::guard('member')->check())
		{
			return redirect('/');
		}
		
		/* Module Payment List */
		// $paymodules = Storage::disk('paylib')->files();
		// $fileArr = array();

		
		// foreach($paymodules as $mod){
			// $fileArr[$mod] = $mod;
		// }
		// debug($fileArr);
		
		return View("login");
	}
	
	
	function getCreator(Request $request){
		$data = User::where('is_creator',1)->paginate(16);
		
		return view('creator',compact('data'));		
	}
	
	function getOrganizer(Request $request){
		$data = Organizer::paginate(16);
		
		return view('organizer',compact('data'));		
	}
	
	
	
}
