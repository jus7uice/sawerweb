<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use View;

class Frontend
{
    
	/**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		if (Auth::guard('member')->check())
		{
			$user = Auth::guard('member')->user();
			/* cek 1 device */
			$login_session = session('login_session_member');
			if($login_session != $user->login_session){
				Auth::logout();	
				Auth::guard('member')->logout();
				session()->flush();
				return redirect()->intended('/login')->withErrors(trans('message.login_other_device_detected'));   
			}	
			
			$request->attributes->add(['auth_user' => json_decode(json_encode(Auth::guard('member')->user()))]);
		}
				
		return $next($request);
    }
}
