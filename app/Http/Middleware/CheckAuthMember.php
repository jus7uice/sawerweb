<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckAuthMember
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::guard('member')->check()) {
            return redirect()->intended('/login')->withErrors("Opss.. Silahkan login terlebih dahulu");   
        }
		
		return $next($request);
    }
}
