<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
    // return view('welcome');
// });




Route::group(['prefix'=>'akun','middleware' => ['frontend','auth.only']], function () {
	
	Route::get('/dashboard', 'DashboardCtr@index');
	// Route::get('/profil', 'UserProfilCtr@index');
		Route::get('/profil/ubah', 'UserProfilCtr@getEdit');
		Route::post('/profil/ubah', 'UserProfilCtr@postEdit');
	Route::get('/pendapatan', 'UserPendapatanCtr@index');
	
	Route::get('/video', 'UserVideoCtr@index');
	Route::get('/video.data', 'UserVideoCtr@getData');
	Route::get('/video/tambah', 'UserVideoCtr@getCreate');
	Route::post('/video/tambah', 'UserVideoCtr@postCreate');
	Route::get('/video/ubah', 'UserVideoCtr@getEdit');
	Route::post('/video/ubah', 'UserVideoCtr@postEdit');
	
	Route::get('/logout', 'Auth\AuthCtr@logout');
	
});



Route::get('/chat', 'AppCtr@getChat');




/* Backend Route */
require '_backend.php';

Route::group(['middleware' => 'frontend'], function () {
	
	Route::get('/', 'AppCtr@index');
	Route::get('/login', 'AppCtr@getLogin');
	Route::post('/login', 'Auth\AuthCtr@postLogin');

	Route::get('/register', 'RegisterCtr@getRegister');
	Route::post('/register', 'RegisterCtr@postRegister');
	Route::get('/aktivasi', 'RegisterCtr@doAktivasi');
	
	Route::get('/organizer/','AppCtr@getOrganizer');
	Route::get('/creator/','AppCtr@getCreator');
	
	Route::get('/video/','VideoCtr@index');
	Route::get('/video/{video_id}','VideoCtr@getVideo');
	
	
	Route::get('/{username}','UserCtr@getUser');


});
