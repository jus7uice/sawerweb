<?php

Route::get(ADMIN_PATH,'Auth\AuthAdminCtr@index')->middleware('cek.auth','cek.ipwhitelist');
Route::get(ADMIN_PATH.'logout','Auth\AuthAdminCtr@logout');

Route::group(['middleware' => 'web'], function () {
	// Route::post(ADMIN_PATH.'authadmin','Auth\AuthAdminCtr@postAuth');
	Route::post(ADMIN_PATH.'authadmin','Auth\AuthAdminCtr@postAuthWithThrottle'); // With throttle
});

Route::group(['prefix'=>ADMIN_PATH, 'namespace'=> 'Backend\\','middleware' => ['auth.admin','cek.ipwhitelist']], function () {
	
	Route::get('err/ip.blocked',function(){return view('special.err_ip_block');});
	Route::get('err/restrical.access',function(){return view('special.err_restrical_access');})->middleware('auth.admin');
	
	Route::get('dashboard','ApplicationCtr@index');
	
	Route::get('setting.general','SettingCtr@index');
	Route::post('setting.general.post','SettingCtr@postGeneral');	

	// Route::get('setting.pg','SettingPgCtr@index');
	// Route::post('setting.pg','SettingPgCtr@postUpdate');	
	// Route::get('setting.pg.gettoken','SettingPgCtr@getToken');	

	/* Aktifkan Jika ingin menggunakan IP Whitelist */
	Route::get('setting.ipwhitelist','IpWhitelistCtr@index');
	Route::get('setting.ipwhitelist.data','IpWhitelistCtr@getData');
	Route::get('setting.ipwhitelist.create','IpWhitelistCtr@getCreate');
	Route::post('setting.ipwhitelist.create','IpWhitelistCtr@postCreate');
	Route::post('setting.ipwhitelist.delete','IpWhitelistCtr@postDelete');
	
	Route::get('setting.theme','ThemeCtr@index');
	Route::post('setting.theme','ThemeCtr@postData');
	
	Route::post('admin.profile.post','AdminCtr@postMe');
	Route::post('admin.profile.chgpass','AdminCtr@postChangePassword');
	Route::get('admin.profile','AdminCtr@getMe');
	
	Route::get('admin.user','AdminCtr@getUsers');
	Route::get('admin.user.data','AdminCtr@getUsersData');
	Route::get('admin.user.create','AdminCtr@createUser');
	Route::post('admin.user.create','AdminCtr@postCreateUser');
	Route::get('admin.user.edit','AdminCtr@editUser');
	Route::post('admin.user.edit','AdminCtr@postEditUser');
	Route::post('admin.user.delete','AdminCtr@postDeleteUsers');
	
	Route::get('admin.group','AdminGroupCtr@index');
	Route::get('admin.group.data','AdminGroupCtr@getGroupsData');
	Route::get('admin.group.create','AdminGroupCtr@createGroup');
	Route::post('admin.group.create','AdminGroupCtr@postCreateGroup');
	Route::get('admin.group.edit','AdminGroupCtr@editGroup');
	Route::post('admin.group.edit','AdminGroupCtr@postEditGroup');
	Route::post('admin.group.delete','AdminGroupCtr@postDeleteGroups');
	
	Route::get('admin.log','AdminLogCtr@index');
	Route::get('admin.log.data','AdminLogCtr@getData');
	
	// Route::get('channel','ChannelCtr@index');
	// Route::get('channel.data','ChannelCtr@getData');
	// Route::get('channel.create','ChannelCtr@getCreate');
	// Route::post('channel.create','ChannelCtr@postCreate');
	// Route::get('channel.edit','ChannelCtr@getEdit');
	// Route::post('channel.edit','ChannelCtr@postEdit');
	// Route::post('channel.delete','ChannelCtr@postDelete');
	
	Route::get('category','CategoryCtr@index');
	Route::get('category.data','CategoryCtr@getData');
	Route::get('category.create','CategoryCtr@getCreate');
	Route::post('category.create','CategoryCtr@postCreate');
	Route::get('category.edit','CategoryCtr@getEdit');
	Route::post('category.edit','CategoryCtr@postEdit');
	Route::post('category.delete','CategoryCtr@postDelete');
	
	Route::get('restrical.username','RestricalUsernameCtr@index');
	Route::get('restrical.username.data','RestricalUsernameCtr@getData');
	Route::get('restrical.username.create','RestricalUsernameCtr@getCreate');
	Route::post('restrical.username.create','RestricalUsernameCtr@postCreate');
	Route::get('restrical.username.edit','RestricalUsernameCtr@getEdit');
	Route::post('restrical.username.edit','RestricalUsernameCtr@postEdit');
	Route::post('restrical.username.delete','RestricalUsernameCtr@postDelete');
	
	Route::get('tag','TagCtr@index');
	Route::get('tag.data','TagCtr@getData');
	Route::get('tag.json','TagCtr@getJson');
	Route::get('tag.create','TagCtr@getCreate');
	Route::post('tag.create','TagCtr@postCreate');
	Route::get('tag.edit','TagCtr@getEdit');
	Route::post('tag.edit','TagCtr@postEdit');
	Route::post('tag.delete','TagCtr@postDelete');
	
	Route::get('user','UserCtr@index');
	Route::get('user.data','UserCtr@getData');
	Route::get('user.create','UserCtr@getCreate');
	Route::post('user.create','UserCtr@postCreate');
	Route::get('user.edit','UserCtr@getEdit');
	Route::post('user.edit','UserCtr@postEdit');
	Route::post('user.delete','UserCtr@postDelete');
	
	Route::get('organizer','OrganizerCtr@index');
	Route::get('organizer.data','OrganizerCtr@getData');
	Route::get('organizer.create','OrganizerCtr@getCreate');
	Route::post('organizer.create','OrganizerCtr@postCreate');
	Route::get('organizer.edit','OrganizerCtr@getEdit');
	Route::post('organizer.edit','OrganizerCtr@postEdit');
	Route::post('organizer.delete','OrganizerCtr@postDelete');
	
	Route::get('post','PostCtr@index');
	Route::get('post.data','PostCtr@getData');
	Route::get('post.create','PostCtr@getCreate');
	Route::post('post.create','PostCtr@postCreate');
	Route::get('post.edit','PostCtr@getEdit');
	Route::post('post.edit','PostCtr@postEdit');
	Route::post('post.delete','PostCtr@postDelete');
	
	Route::post('media.upload','MediaCtr@postUpload');
	Route::get('media.json','MediaCtr@getData');	
	Route::get('media','MediaManagerCtr@index');
	Route::get('media.data','MediaManagerCtr@getData');
	Route::get('media.create','MediaManagerCtr@getCreate');
	Route::post('media.create','MediaManagerCtr@postCreate');
	Route::get('media.edit','MediaManagerCtr@getEdit');
	Route::post('media.edit','MediaManagerCtr@postEdit');
	Route::post('media.delete','MediaManagerCtr@postDelete');
	Route::get('media.delete','MediaManagerCtr@getDelete');
	
	Route::get('slideshow','SlideshowCtr@index');
	Route::get('slideshow.data','SlideshowCtr@getData');
	Route::get('slideshow.create','SlideshowCtr@getCreate');
	Route::post('slideshow.create','SlideshowCtr@postCreate');
	Route::get('slideshow.edit','SlideshowCtr@getEdit');
	Route::post('slideshow.edit','SlideshowCtr@postEdit');
	Route::post('slideshow.delete','SlideshowCtr@postDelete');
	
	/* Paket Koin / Coins */
	Route::get('coins.package','CoinsPackageCtr@index');
	Route::get('coins.package.data','CoinsPackageCtr@getData');
	Route::get('coins.package.create','CoinsPackageCtr@getCreate');
	Route::post('coins.package.create','CoinsPackageCtr@postCreate');
	Route::get('coins.package.edit','CoinsPackageCtr@getEdit');
	Route::post('coins.package.edit','CoinsPackageCtr@postEdit');
	Route::post('coins.package.delete','CoinsPackageCtr@postDelete');
	Route::get('coins.package.delete','CoinsPackageCtr@getDelete');
	
	/* Sticker Stock */
	Route::get('sticker.stock','StickerStockCtr@index');
	Route::get('sticker.stock.data','StickerStockCtr@getData');
	Route::get('sticker.stock.create','StickerStockCtr@getCreate');
	Route::post('sticker.stock.create','StickerStockCtr@postCreate');
	Route::get('sticker.stock.edit','StickerStockCtr@getEdit');
	Route::post('sticker.stock.edit','StickerStockCtr@postEdit');
	Route::post('sticker.stock.delete','StickerStockCtr@postDelete');
	Route::get('sticker.stock.delete','StickerStockCtr@getDelete');
	
	/* Payment Provider */
	Route::get('payment.provider','PaymentProviderCtr@index');
	Route::get('payment.provider.data','PaymentProviderCtr@getData');
	Route::get('payment.provider.create','PaymentProviderCtr@getCreate');
	Route::post('payment.provider.create','PaymentProviderCtr@postCreate');
	Route::get('payment.provider.edit','PaymentProviderCtr@getEdit');
	Route::post('payment.provider.edit','PaymentProviderCtr@postEdit');
	Route::post('payment.provider.delete','PaymentProviderCtr@postDelete');
	Route::get('payment.provider.delete','PaymentProviderCtr@getDelete');
	
	Route::get('payment.provider.gettoken','PaymentProviderCtr@getGetToken');
	Route::post('payment.provider.gettoken','PaymentProviderCtr@postGetToken');
	
	
	/* Payment Method */
	Route::get('payment.method','PaymentMethodCtr@index');
	Route::get('payment.method.data','PaymentMethodCtr@getData');
	Route::get('payment.method.create','PaymentMethodCtr@getCreate');
	Route::post('payment.method.create','PaymentMethodCtr@postCreate');
	Route::get('payment.method.edit','PaymentMethodCtr@getEdit');
	Route::post('payment.method.edit','PaymentMethodCtr@postEdit');
	Route::post('payment.method.delete','PaymentMethodCtr@postDelete');
	Route::get('payment.method.delete','PaymentMethodCtr@getDelete');
	
	
	
	
	
});


?>