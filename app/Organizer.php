<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organizer extends Model
{

	protected $guarded = [];
		
	public function user()
    {
        // return 'Admin Group';
		return $this->belongsTo('\App\User', 'user_id');
    }
	
	public function users()
    {
        // return 'Admin Group';
		return $this->hasMany('\App\User', 'user_id');
    }
	

	
}
