<!doctype html>
<html>
  <head>
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>{{$param['title'] }}</title>
    <style>
    /* -------------------------------------
        INLINED WITH htmlemail.io/inline
    ------------------------------------- */
    /* -------------------------------------
        RESPONSIVE AND MOBILE FRIENDLY STYLES
    ------------------------------------- */
    @media only screen and (max-width: 620px) {
      table[class=body] h1 {
        font-size: 28px !important;
        margin-bottom: 10px !important;
      }
      table[class=body] p,
            table[class=body] ul,
            table[class=body] ol,
            table[class=body] td,
            table[class=body] span,
            table[class=body] a {
        font-size: 16px !important;
      }
      table[class=body] .wrapper,
            table[class=body] .article {
        padding: 10px !important;
      }
      table[class=body] .content {
        padding: 0 !important;
      }
      table[class=body] .container {
        padding: 0 !important;
        width: 100% !important;
      }
      table[class=body] .main {
        border-left-width: 0 !important;
        border-radius: 0 !important;
        border-right-width: 0 !important;
      }
      table[class=body] .btn table {
        width: 100% !important;
      }
      table[class=body] .btn a {
        width: 100% !important;
      }
      table[class=body] .img-responsive {
        height: auto !important;
        max-width: 100% !important;
        width: auto !important;
      }
    }

    /* -------------------------------------
        PRESERVE THESE STYLES IN THE HEAD
    ------------------------------------- */
    @media all {
      .ExternalClass {
        width: 100%;
      }
      .ExternalClass,
            .ExternalClass p,
            .ExternalClass span,
            .ExternalClass font,
            .ExternalClass td,
            .ExternalClass div {
        line-height: 100%;
      }
      .apple-link a {
        color: inherit !important;
        font-family: inherit !important;
        font-size: inherit !important;
        font-weight: inherit !important;
        line-height: inherit !important;
        text-decoration: none !important;
      }
      #MessageViewBody a {
        color: inherit;
        text-decoration: none;
        font-size: inherit;
        font-family: inherit;
        font-weight: inherit;
        line-height: inherit;
      }
      .btn-primary table td:hover {
        background-color: #34495e !important;
      }
      .btn-primary a:hover {
        background-color: #34495e !important;
        border-color: #34495e !important;
      }
    }
    </style>
  </head>
  <body class="" style=" background: #ffffff; background-color: #ffffff; font-family: sans-serif; -webkit-font-smoothing: antialiased; font-size: 14px; line-height: 1.4; margin: 0; padding: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
  
	@if(isset($param['preview_mode']) && $param['preview_mode'])
	<table border="0" cellpadding="0" cellspacing="0" class="body" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background-color: #ff9999;">
	
      <tr>
		<td>
			<p style="padding: 5px; text-align: center;">
				This is preview mode <br />
			</p>
		</td>
	  </tr>
	</table><br /><br />
	
	@endif
  
  
    <table border="0" cellpadding="0" cellspacing="0" class="body" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background-color: #ffffff;  background: #ffffff;  background-image: url('https://i.ibb.co/crR7Gc6/bg-white.jpg'); padding: 10px;">
	
      <tr>
        <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">&nbsp;</td>
        <td class="container" style="font-family: sans-serif; font-size: 14px; vertical-align: top; display: block; /* Margin: 0 auto;*/ max-width: 580px; padding: 10px; width: 580px;">
		
          <div class="content" style="box-sizing: border-box; display: block; Margin: 0 auto; max-width: 580px;">
			<p>
			<center>
			<!--
			<img src="https://i.ibb.co/HDzLfVh/logo-growinc-technology-medium.png" alt="growinc technology" style="border: 0;line-height: 100%;vertical-align: middle;text-align: center;height: 40px;">
			-->
				<h1>SAWER.ID</h1>
			</center>
			<p/>
			
            <!-- START CENTERED WHITE CONTAINER -->
			<!--<span class="preheader" style="color: transparent; display: none; height: 0; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; mso-hide: all; visibility: hidden; width: 0;">Terjadi kendala dalam menampilkan isi pesan.</span>
			-->
			<p>&nbsp;</p>
			
			<div class="content" style="display: block; Margin: 0 auto; border-top: 2px solid #A1CC3A; border-radius: 5px; moz-border-radius: 5px; padding: 10px; background-color: #fcfcfc; ">
				<table class="main" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; border-radius: 3px;">

					<!-- START MAIN CONTENT AREA -->
					<tr>
					<td class="wrapper" style="font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box;">
					  <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; padding-bottom: 15px; border-bottom: 1px solid #aeaeae;">
						<tr>
						  <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">
						   <p><b>Hi {{$param['recepient_name']??"-"}}</b>, <p>
						   
						   @if(isset($param["reference_no"]))
						   <p>Telah diterima pembayaran sebesar {{$param['currency']??""}} {{$param['amount']??"0.00"}} dengan rincian sebagai berikut: <p>
						   @endif
						   
						  </td>
						</tr>
						<tr>
						  <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">
							   <p>{!!$param['content']??""!!}<p>
							
								<!-- Detail -->
								<table border="0" width="100%" cellspacing="0" cellpadding="0">
	
									@if(isset($param["aktivasi_url"]))
									<tr>
										<td>Aktivasi</td>
										<td>:</td>
										<td>{{$param["aktivasi_url"]}}</td>
									</tr>	
									@endif
									
									@if(isset($param["reference_no"]))
									<tr>
										<td>Reference No</td>
										<td>:</td>
										<td>{{$param["reference_no"]}}</td>
									</tr>	
									@endif
									
									@if(isset($param["invoice_no"]))
									<tr>
										<td>Invoice No</td>
										<td>:</td>
										<td>{{$param["invoice_no"]}}</td>
									</tr>	
									@endif
									
									@if(isset($param["pay_method"]))
									<tr>
										<td>Metode Pembayaran</td>
										<td>:</td>
										<td>{{$param["pay_method"]}}</td>
									</tr>	
									@endif
									
									@if(isset($param["pay_code"]))
									<tr>
										<td>Kode Bayar</td>
										<td>:</td>
										<td>{{$param["pay_code"]}}</td>
									</tr>	
									@endif
									
									
									@if(isset($param["amount"]))
									<tr>
										<td>Jumlah</td>
										<td>:</td>
										<td>{{$param["currency"]??""}} {{$param["amount"]}}</td>
									</tr>	
									@endif
									
									
									@if(isset($param["paid_at"]))
									<tr>
										<td>Tanggal bayar</td>
										<td>:</td>
										<td>{{$param["paid_at"]??""}}</td>
									</tr>	
									@endif
								</table>
								
								@if(isset($param["change_password"]))
								<!-- Info Transaksi -->
								<center>
									
									<p>Password akun Anda telah berhasil diubah dan diperbarui.</p>
									<p>Jika Anda tidak melakukan perubahan ini, harap segera hubungi administrator Anda.</p>
							
								</center>
								@endif
							
						   
						  </td>
						</tr>
					  </table>
					</td>
					</tr>
					<!-- END MAIN CONTENT AREA -->
				</table>
			</div>
				
		
            <!-- START FOOTER -->
            <div class="footer" style="clear: both; Margin-top: 10px; text-align: center; width: 100%;">
              <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
                <tr>
                  <td class="content-block" style="font-family: sans-serif; vertical-align: top; padding-bottom: 10px; padding-top: 10px; font-size: 11px; color: #676767; text-align: center;">
                   This email was generated automatically by the system. Please do not reply to this email.
                  </td>
                </tr>
                <tr>
                  <td class="content-block powered-by" style="font-family: sans-serif; vertical-align: top; padding-bottom: 10px; padding-top: 10px; font-size: 11px; color: #676767; text-align: center;">
                    Cpopyright &copy; 2020 <a href="{{URL('/')}}" style="color: #A1CC3A; font-size: 12px; text-align: center; text-decoration: none;">{{APP_NAME}}</a>.
                  </td>
                </tr>
              </table>
            </div>
            <!-- END FOOTER -->

          <!-- END CENTERED WHITE CONTAINER -->
          </div>
		  
		  
        </td>
        <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">&nbsp;</td>
      </tr>
    </table>
  </body>
</html>
