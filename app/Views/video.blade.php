@extends('layouts.web')

@section('page_title')
Home
@endsection

@section('content')
<!-- First Container -->
<div class="container">

	<div class="row row-no-gutters">  
		
		<div class="col-sm-12 text-center">
			{{ $data->links() }}
		</div>
		<div class="col-sm-12">
			@foreach($data as $row)
			
			
			<div class="col-xl-12 col-sm-3">
				<a href="{{url('/video/'.$row->uuid)}}">
					<div class="panel panel-default no-margin margin">
					  
						<div class="embed-container">
						<img src="{{ LaravelVideoEmbed::getYoutubeThumbnail($row->video_url) }}" class="">
						</div>
					</div>
				</a>
				
			</div>
			@endforeach
		</div>
		<div class="col-sm-12 text-center">
			{{ $data->links() }}
		</div>
	
	</div>
</div>

@endsection

@section('js')
  <script>
  $(function(){
	 $('.slickme').slick({
		lazyLoad: 'ondemand',
		// dots: true,
		infinite: true,
		// speed: 300,
		slidesToShow: 4,
		slidesToScroll: 4,
		centerMode: true,
		// variableWidth: true
	 }); 
  });
  </script>
@endsection

@section('css')
<style>
	.panel.no-margin{
		margin: 0px;
	}
	.panel.margin{
		margin: 0px 0px 10px 10px;
	}
	.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
	.embed-container iframe, .embed-container object, .embed-container embed, .embed-container img { position: absolute; top: 0; left: 0; width: 100%; height: 100%;}  
  
	.video-testimonial-block { position: relative; width: auto; height: 206px; overflow: hidden; margin-bottom: 30px; }
	.video-testimonial-block .video-thumbnail { height: 100%; width: 100%; position: absolute; z-index: 1; background-size: cover; top: 0; left: 0; }
	.video-testimonial-block .video { }
	.video-testimonial-block .video iframe { position: absolute; top: 0; left: 0; width: 100%; height: 100%; border: 0px; }
	.video-testimonial-block .video-play { position: absolute; z-index: 2; top: 50%; left: 50%; margin-left: -40px; margin-top: -18px; text-decoration: none; }
	.video-testimonial-block .video-play::before { content: "\f144"; font: normal normal normal 14px/1; font-family: 'Font Awesome\ 5 Free'; font-weight: 900; font-size: inherit; text-rendering: auto; -webkit-font-smoothing: antialiased; -moz-osx-font-smoothing: grayscale; font-size: 50px; color: #b3b5bc; }
	.video-testimonial-block .video-play:hover::before { color: #172651; }
</style>
@endsection
