<!DOCTYPE html>
<html lang="en">
<head>
  <title>@yield('page_title'){{APP_NAME}}</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.css" integrity="sha512-wR4oNhLBHf7smjy0K4oqzdWumd+r5/+6QO/vDda76MW5iug4PT7v86FoEkySIJft3XA0Ae6axhIvHrqwm793Nw==" crossorigin="anonymous" />
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.1.0/css/all.css" integrity="sha512-ajhUYg8JAATDFejqbeN7KbF2zyPbbqz04dgOLyGcYEk/MJD3V+HJhJLKvJ2VVlqrr4PwHeGTTWxbI+8teA7snw==" crossorigin="anonymous" />
  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pace/0.7.8/pace.min.js" integrity="sha512-t3TewtT7K7yfZo5EbAuiM01BMqlU2+JFbKirm0qCZMhywEbHZWWcPiOq+srWn8PdJ+afwX9am5iqnHmfV9+ITA==" crossorigin="anonymous"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js" integrity="sha512-XtmMtDEcNz2j7ekrtHvOVR4iwwaD6o/FUJe6+Zq+HgcCsk3kj4uSQQR8weQ2QVj1o0Pk6PwYLohm206ZzNfubg==" crossorigin="anonymous"></script>
  
  <style>
  body {
    font: 14px Montserrat, sans-serif;
    line-height: 1.8;
	padding-top : 50px;
  }
  .margin {margin-bottom: 45px;}
 
/*   .container-fluid {
    padding-top: 70px;
    padding-bottom: 70px;
  } */
  .navbar {
    padding-top: 15px;
    padding-bottom: 15px;
    border: 0;
    border-radius: 0;
    margin-bottom: 0;
    font-size: 12px;
    letter-spacing: 5px;
  }
  .navbar-nav  li a:hover {
    color: #1abc9c !important;
  }
	
	.pace {
	  -webkit-pointer-events: none;
	  pointer-events: none;

	  -webkit-user-select: none;
	  -moz-user-select: none;
	  user-select: none;
	}

	.pace-inactive {
	  display: none;
	}

	.pace .pace-progress {
	  background: #29d;
	  position: fixed;
	  z-index: 2000000;
	  top: 0;
	  width: 100%;
	  height: 2px;
	}

	  .bg-footer{ 
		background-color: #2f2f2f; /* Black Gray */
		color: #fff;
	  }
  
  </style>
  @yield('css')
  
</head>
<body>
<img data-lazy="https://media.giphy.com/media/3AMRa6DRUhMli/giphy.gif"/>

<!-- Navbar -->
<nav class="navbar navbar-default  navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="{{url('/')}}">{{APP_NAME}}</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
	
	
      <ul class="nav navbar-nav">
        <li><a href="{{url('/video')}}"><i class="fa fa-video"></i> VIDEO</a></li>
        <li><a href="#"><i class="fa fa-user"></i> CREATOR</a></li>
        <li><a href="#"><i class="fa fa-home"></i> ORGANIZER</a></li>		
		
      </ul>
	  
      <ul class="nav navbar-nav navbar-right">      
		@if(!Request()->get('auth_user'))
        <li><a href="{{url('login')}}"><i class="fa fa-key"></i> LOGIN</a></li>
		@endif
		
		@if(Request()->get('auth_user'))
        <li><a href="{{url('/akun/koin')}}" class="" title="Koin saya"><i class="fa fa-coins"></i><strong> {{Request()->get('auth_user')->coins_sum}}</strong> | </a></li>
		<li class="dropdown">
			<a class="dropdown-toggle" data-toggle="dropdown" href="#">Hi, <strong>{{Request()->get('auth_user')->name}}</strong> 
			<span class="caret"></span></a>
			<ul class="dropdown-menu">
				<li><a href="{{url('akun/dashboard')}}">Dashboard</a></li>
				<li><a href="{{url('akun/profil')}}">Profil</a></li>
				<li><a href="{{url('akun/video')}}">Koleksi Video</a></li>
				<li><a href="{{url('akun/pendapatan')}}">Pendapatan</a></li>
				<li class="divider"></li>
				<li><a href="{{url('akun/logout')}}">Logout</a></li> 
			</ul>
		</li>
		@endif
      </ul>
    </div>
  </div>
</nav>

<p>&nbsp;</p>

<div class="container">
@if (session()->has('email_verification'))	<div class="alert alert-success">Registrasi berhasil. Silahkan cek email dan lakukan konfirmasi aktivasi akun Anda</div>@endif				
@if (session()->has('msg'))	<div class="alert alert-success">{!!session()->get('msg')!!}</div>@endif				
		
@if ($errors->any())
	<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
@endif
</div>
<!-- Web Container -->
@yield('content')

<!-- Footer -->
<footer class="container-fluid bg-footer text-center">
  <p>&copy; 2020 - <a href="{{APP_URL}}">{{APP_NAME}}</a> </p> 
</footer>

	@yield('js')
	
</body>
</html>
