<?php 
$body = '
<form role="form" action="'.url(ADMIN_PATH.'payment.provider.edit').'" method="post" id="ajxForm">
  <div class="box-body">
	
	<div class="input-group">
	  <span class="input-group-addon">'.trans('general.label.name').' *</span>
	  <input type="text" class="form-control" name="name" value="'.$data->name.'" />
	</div>
	
	<div class="input-group">
	  <span class="input-group-addon">API URL *</span>
	  <input type="text" class="form-control" name="api_url" value="'.$data->api_url.'" />
	</div>
	
	<div class="input-group">
	  <span class="input-group-addon">API Key </span>
	  <input type="text" class="form-control" name="api_key" value="'.$data->api_key.'" />
	</div>
	
	<div class="input-group">
	  <span class="input-group-addon">API Secret </span>
	  <input type="text" class="form-control" name="api_secret" value="'.$data->api_secret.'" />
	</div>
	
	<div class="input-group">
	  <span class="input-group-addon">Client ID </span>
	  <input type="text" class="form-control" name="client_id" value="'.$data->client_id.'" />
	</div>
	
	<div class="input-group">
	  <span class="input-group-addon">Client Secret </span>
	  <input type="text" class="form-control" name="client_secret" value="'.$data->client_secret.'" />
	</div>
	
	<hr />
	
	<div class="input-group">
	  <span class="input-group-addon">Module Name </span>
	  '.Form::select('module_name',$payFiles,$data->module_name,['class'=>'form-control']).'
	</div>
	

  </div>
  <!-- /.box-body -->

  <div class="box-footer">
	<button type="submit" class="btn btn-primary btn-success">'.trans('general.button.save').'</button>
	  '.Form::hidden('id',$data->id).'
	  '.csrf_field().'
  </div>
</form>
';

?>


@include('modal.modal',['title'=>trans('general.label.edit').' | Payment Provider', 'body'=>$body])