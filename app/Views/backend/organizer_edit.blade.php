<?php 
$body = '
<form role="form" action="'.url(ADMIN_PATH.'organizer.edit').'" method="post" id="ajxForm">
  <div class="box-body">
	<div class="input-group">
	 <span class="input-group-addon">'.trans('general.label.owner').' *</span>
	 '.Form::select('user_id', $userList, $data->user_id,['class'=>'form-control']).'
	</div>
	
	<div class="input-group">
	  <span class="input-group-addon">'.trans('general.label.name').' *</span>
	  <input type="text" class="form-control" name="title" value="'.$data->title.'">
	</div>
	
	<div class="form-group">
	   '.Form::hidden('is_official',0).'
	   '.Form::checkbox('is_official',1,($data->is_official==1)?true:false).'
		 <label>'.trans('general.label.is_official').' *</label>
	</div>
			
  </div>
  <!-- /.box-body -->

  <div class="box-footer">
	<button type="submit" class="btn btn-primary btn-success">'.trans('general.button.save').'</button>
	  '.csrf_field().'
	  '.Form::hidden('id',request()->id).'
  </div>
</form>
';

?>


@include('modal.modal',['title'=>trans('general.label.edit').' | Organizer' , 'body'=>$body])