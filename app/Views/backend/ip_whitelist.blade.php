@extends('layouts.backend')

{{-- Title --}}
@section('title')
	- {{trans('general.setting.ipwhitelist')}}
@endsection

{{-- Page title --}}
@section('pagetitle')
	{{trans('general.setting.setting')}} <small>{{trans('general.setting.ipwhitelist')}}</small>
@endsection

@section('content')
	
	<!-- Main content -->
    
      <div class="row">
        
		<div class="col-md-6">		
			<p><a class="btn btn-primary" href="{{url(ADMIN_PATH.'setting.ipwhitelist.create')}}" data-toggle="ajaxModal" >{{trans('general.label.add_new')}}</a></p>
        </div>
		
		<div class="col-md-12">
			<div class="box box-primary">
				<form role="form" action="{{url(ADMIN_PATH.'setting.ipwhitelist.delete')}}" method="post" id="ajxFormDelete">
				<div class="box-header with-border">
				  <h3 class="box-title">{{trans('general.setting.ipwhitelist')}}</h3>
				</div>
				<div class="box-body">
				
					<table id="dataTbl" class="table table-bordered table-hover datatable" data-ajax="{{url(ADMIN_PATH.'setting.ipwhitelist.data')}}" data-processing="true" data-server-side="true" data-orderingxxx="false" data-length-menu="[50,100,500]">
						<thead>
						<tr>
						  <th data-data="chkbox" data-orderable="false"><input type="checkbox" id="select-all" /></th>
						  <th data-data="ip">IP Address</th>
						</tr>
						</thead>
						
					</table>
				
				</div>
				 <div class="box-footer">
					<button type="submit" class="btn btn-danger">{{trans('general.button.delete')}}</button>
					 {{csrf_field()}}
				</div>
				</form>	
			</div>
		</div>
      </div>
      <!-- /.row -->

@endsection