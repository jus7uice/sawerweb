<?php 
$body = '
<form role="form" action="'.url(ADMIN_PATH.'user.edit').'" method="post" id="ajxForm">
  <div class="box-body">
	
	<div class="input-group">
	 <span class="input-group-addon">UUID</span>
	 <input type="email" class="form-control" disabled name="email" value="'.$data->uid.'" />
	</div>
	
	<div class="input-group">
	 <span class="input-group-addon">'.trans('general.organizer.organizer').' *</span>
	 '.Form::select('organizer_id', $organizerList, $data->organizer_id,['class'=>'form-control']).'
	</div>

	<div class="input-group">
	  <span class="input-group-addon">'.trans('general.people.email').' *</span>
	  <input type="email" class="form-control" name="email" value="'.$data->email.'" />
	</div>
		
	
	<div class="input-group">
	  <span class="input-group-addon">'.trans('general.user.name').' *</span>
	  <input type="text" class="form-control" name="name" value="'.$data->name.'" />
	</div>
	
	
	<div class="input-group">
	  <span class="input-group-addon">Nickname *</span>
	  <input type="text" class="form-control" name="nickname" value="'.$data->nickname.'" />
	</div>
	
	<hr />
	
	<div class="input-group">
	  <span class="input-group-addon">'.trans('general.people.password').' *</span>
	  <input type="password" class="form-control" name="password">
	</div>
	<span class="help-block text-orange">'.trans('message.comment_change_password').' *</span>
	<hr />
	
	<div class="form-group">
	  <label> '.Form::hidden('is_creator',0).'
		'.Form::checkbox('is_creator',1,($data->is_creator==1)?true:false).'
		 Creator *</label>
	</div>
	
	<div class="form-group">
	   <label> '.Form::hidden('email_verified_at',null).'
		'.Form::checkbox('email_verified_at', $data->created_at,($data->email_verified_at!="")?true:false).'
		Verified *</label>
	</div>
	
	<div class="form-group">
	   <label>'.Form::hidden('status',0).'
		'.Form::checkbox('status',1,($data->status==1)?true:false).'
		 '.trans('general.label.is_active').' *</label>
	</div>
	
  </div>
  <!-- /.box-body -->

  <div class="box-footer">
	<button type="submit" class="btn btn-primary btn-success">'.trans('general.button.save').'</button>
	  '.csrf_field().'
	   '.Form::hidden('id',Request()->id).'
  </div>
</form>
';

?>


@include('modal.modal',['title'=>trans('general.label.edit').' | '. trans('general.user.user'), 'body'=>$body])