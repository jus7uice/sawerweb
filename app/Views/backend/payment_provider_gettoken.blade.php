@extends('layouts.backend')

{{-- Title --}}
@section('title')
	- Setting PG
@endsection

{{-- Page title --}}
@section('pagetitle')
	
@endsection

@section('content')
	
	<!-- Main content -->
    
      <div class="row">
        
		<div class="col-md-6">
			<div class="box box-primary">
				
					<div class="box-header with-border">
					  <h3 class="box-title">Info</h3>
					</div>
					<div class="box-body">
					
						<div class="form-group">
							<label class="col-sm-2 control-label">API URL</label>
							<div class="col-sm-10">
							  <p class="form-control-static">{{ $data->api_url }}</p>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-2 control-label">API Key</label>
							<div class="col-sm-10">
							  <p class="form-control-static">{{ $data->api_key }}</p>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-2 control-label">API Secret</label>
							<div class="col-sm-10">
							  <p class="form-control-static">{{ $data->api_secret }}</p>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-2 control-label">Client ID</label>
							<div class="col-sm-10">
							  <p class="form-control-static">{{ $data->client_id }}</p>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-2 control-label">Client Secret</label>
							<div class="col-sm-10">
							  <p class="form-control-static">{{ $data->client_secret }}</p>
							</div>
						</div>		
					</div>
				
			</div>
		</div>
     
        
		<div class="col-md-6">
			<div class="box box-primary">
				<form role="form" action="{{url(ADMIN_PATH.'payment.provider.gettoken')}}" method="post" enctype="multipart/form-data">
					<div class="box-header with-border">
					  <h3 class="box-title">Generate Token</h3>
					</div>
					<div class="box-body">
					{!!$data->token??'<i>No token available</i>'!!}
						
					<p>&nbsp;</p>
					Expire In {{$data->token_expired_at}}
										
					</div>
					 <div class="box-footer">
						<button type="submit" class="btn btn-success">Generate New Token</button>
						 {{Form::hidden('id',$data->id)}}
						 {{csrf_field()}}
					</div>
				</form>	
			</div>
		</div>
      </div>
      <!-- /.row -->

@endsection









