@extends('layouts.backend')

{{-- Title --}}
@section('title')
	- {{trans('general.admin.logs')}}
@endsection

{{-- Page title --}}
@section('pagetitle')
	{{trans('general.admin.logs')}}
@endsection

@section('js')
<script>
  $(function (){
	   $('#date_from,#date_to').combodate({
		    minYear: 2017,
			maxYear: {{date("Y")}},
	   });
  });
</script>
@endsection

@section('content')

	<!-- Main content -->
    
      <div class="row">        
		
		<div class="col-md-12 col-md-offset-0">
			<div class="box box-danger @if(!Request()->query())collapsed-box @endif">
			
				<form action="" method="get" class="form-inline">
					<div class="box-header with-border" data-widget="collapse">
						<h3 class="box-title">{{trans('general.label.search')}}</h3>
						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool"><i class="fa fa-plus"></i></button>
						</div>
					</div>
					<div class="box-body">
					
						<div class="form-group">
							<label>{{trans('general.label.date')}} | {{trans('general.label.from')}} </label>
							<input type="text" data-custom-class="form-control input-sm" id="date_from" name="date_from" value="{{$date_from}}" data-format="DD-MM-YYYY" data-template="D MMM YYYY">
							<label>  &nbsp;&nbsp;&nbsp;{{trans('general.label.to')}} &nbsp;&nbsp;&nbsp; </label>
							<input type="text" data-custom-class="form-control input-sm" id="date_to" name="date_to" value="{{$date_to}}" data-format="DD-MM-YYYY" data-template="D MMM YYYY">
							&nbsp;&nbsp;&nbsp;							
						</div>
						
						<div class="form-group">
						  <label>{{trans('general.label.method')}}</label>
							  {{Form::select('method[]',['GET'=>'GET','POST'=>'POST'],Request()->method,['class'=>'form-control multiselect','multiple'=>'multiple'])}}
						</div>
					
					</div>
					 <div class="box-footer">
						<button type="submit" class="btn btn-success">{{trans('general.button.search')}}</button>
						<button type="button" class="btn btn-warning pull-right" onclick="location.href='{{Request()->url()}}'">{{trans('general.button.reset')}}</button>
					</div>
				</form>
			</div>
		</div>
      </div>
      <!-- /.row -->
		
	<div class="row">        
		
		<div class="col-md-12">
			<div class="box box-primary">
				<div class="box-header with-border">
				  <h3 class="box-title">{{trans('general.admin.user')}}</h3>
				</div>
				<div class="box-body">
				
					<table id="dataTbl" class="table table-bordered table-hover datatable" data-ajax="{{url(ADMIN_PATH.'admin.log.data'.$qs)}}" data-processing="true" data-server-side="true" data-state-savexxx="true" data-ordering="true" data-length-menu="[50,100,500]">
						<thead>
						<tr>
						  <th data-data="created_at">{{trans('general.label.date')}}</th>
						  <th data-data="method">{{trans('general.label.method')}}</th>
						  <th data-data="description">{{trans('general.label.description')}}</th>
						  <th data-data="ip">IP</th>
						  <th data-data="name">{{trans('general.label.user')}}</th>
						</tr>
						</thead>
						
					</table>
				
				</div>
				
			</div>
		</div>
      </div>
      <!-- /.row -->

@endsection