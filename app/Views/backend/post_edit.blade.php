@extends('layouts.backend')

{{-- Title --}}
@section('title')
	- {{trans('general.label.add_new')}} | {{trans('general.post.post')}}
@endsection

{{-- Page title --}}
@section('pagetitle')
	
@endsection

@section('content')
	
	<!-- Main content -->
    
      <div class="row">
        
		<div class="col-md-8 col-md-offset-2">
			<div class="box box-warning">
				<form role="form" action="{{url(ADMIN_PATH.'post.edit')}}" method="post" enctype="multipart/form-data">
					<div class="box-header with-border">
					  <h3 class="box-title">{{trans('general.label.edit')}} | {{trans('general.post.post')}}</h3>
					</div>
					<div class="box-body">
						
						<div class="input-group">
							 <span class="input-group-addon">{{trans('general.category.category')}} *</span>
							 {{Form::select('category_id', $categoryList, $data->category_id,['class'=>'form-control'])}}
						</div>
					
						<div class="input-group">
						  <span class="input-group-addon">{{trans('general.post.title')}} *</span>
						  <input type="text" class="form-control" name="title" value="{{$data->title}}">
						</div>
						
						<div class="input-group">
						  <span class="input-group-addon">Video URL *</span>
						  <input type="text" class="form-control" name="video_url" value="{{$data->video_url}}">
						</div>
						
						<div class="form-group">
							<label>{{trans('general.label.description')}}</label>
							 <div class="form-line">
								  <textarea id="" class="editor form-control" name="description" rows="7">{!!str_decode($data->description)!!}</textarea>
							 </div>
						</div>
						
						<div class="form-group input-group">
							<span class="input-group-addon">{{trans('general.tag.tag')}} *</span>
							<select class="form-control tag" multiple="multiple" data-placeholder="Select #tag" name="tag[]">
								@foreach($PostTagList as $key=>$val)
									<option value="{{$key}}" selected="selected">{{$val}}</option>								
								@endforeach
							</select>						  
						</div>
						
						<div class="form-group">
							<label>{{trans('general.label.is_active')}}</label>
							<div class="checkbox">
								{{Form::hidden('status',0)}}
								<label>
								{{Form::checkbox('status',1,($data->status==1)?true:false)}}
								 {{trans('general.label.is_active')}} *
								 </label>
							</div>
						</div>
					
					</div>
					 <div class="box-footer">
						<button type="submit" class="btn btn-success">{{trans('general.button.save')}}</button>
						 {{csrf_field()}}
						 {{Form::hidden('id',Request()->id)}}
					</div>
				</form>	
			</div>
		</div>
      </div>
      <!-- /.row -->

@endsection

 
@section('css')
<link rel="stylesheet" href="{{asset('plugins/redactor/redactor.css')}}" type="text/css">
<!-- Select2 -->
<link rel="stylesheet" href="{{asset('adminlte/bower_components/select2/dist/css/select2.min.css')}}" type="text/css">
@stop
@section('js')
<!-- Rdc -->
<script src="{{asset('plugins/redactor/redactor.js')}}"></script>
<script src="{{asset('plugins/redactor/table.js')}}"></script>
<script src="{{asset('plugins/redactor/fontfamily.js')}}"></script>
<script src="{{asset('plugins/redactor/fontcolor.js')}}"></script>
<script src="{{asset('plugins/redactor/fullscreen.js')}}"></script>
<script src="{{asset('plugins/redactor/imagemanager.js')}}"></script>
<script src="{{asset('plugins/redactor/fontsize.js')}}"></script>
<!-- Select2 -->
<script src="{{asset('adminlte/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
<script>
$(function(){
	/* wysiwyg */
	$('.editor').redactor({
		replaceDivs: false,
		focus: true,
		imageUpload: "{{url(ADMIN_PATH.'media.upload?_token='.csrf_token())}}",
		imageManagerJson: "{{url(ADMIN_PATH.'media.json')}}",
		plugins: ['table','fontsize','imagemanager','fontfamily','fontcolor','fullscreen'],
		minHeight: 300 // pixels
	});
	
	//Initialize Select2 Elements
    $('.tag').select2({
		ajax: {
			minimumInputLength: 2,
			url: "{{url(ADMIN_PATH.'tag.json')}}",
			dataType: 'json',
			delay: 250,
			processResults: function (data) {
				return {results: data };
			},

		}
	});
	
});
</script>

@stop








