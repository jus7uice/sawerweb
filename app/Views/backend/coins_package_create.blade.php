<?php 
$body = '
<form role="form" action="'.url(ADMIN_PATH.'coins.package.create').'" method="post" id="ajxForm">
  <div class="box-body">
	
	<div class="input-group">
	  <span class="input-group-addon">'.trans('general.label.title').' *</span>
	  <input type="text" class="form-control" name="title">
	</div>
	
	<div class="input-group">
	  <span class="input-group-addon">Price *</span>
	  <input type="text" class="form-control" name="price">
	</div>
	
	<div class="input-group">
	  <span class="input-group-addon">Total Coins *</span>
	  <input type="text" class="form-control" name="total_coins">
	</div>
	
	<div class="form-group">
	   '.Form::hidden('is_fav',0).'
	   '.Form::checkbox('is_fav',1).'
		 <label>Is Favorite? *</label>
	</div>
			
  </div>
  <!-- /.box-body -->

  <div class="box-footer">
	<button type="submit" class="btn btn-primary btn-success">'.trans('general.button.save').'</button>
	  '.csrf_field().'
  </div>
</form>
';

?>


@include('modal.modal',['title'=>trans('general.label.add_new').' | Coins Package', 'body'=>$body])