<?php 
$body = '
<form role="form" action="'.url(ADMIN_PATH.'payment.provider.create').'" method="post" id="ajxForm">
  <div class="box-body">
	
	<div class="input-group">
	  <span class="input-group-addon">'.trans('general.label.name').' *</span>
	  <input type="text" class="form-control" name="name">
	</div>
	
	<div class="input-group">
	  <span class="input-group-addon">API URL *</span>
	  <input type="text" class="form-control" name="api_url">
	</div>
	
	<div class="input-group">
	  <span class="input-group-addon">API Key </span>
	  <input type="text" class="form-control" name="api_key">
	</div>
	
	<div class="input-group">
	  <span class="input-group-addon">API Secret </span>
	  <input type="text" class="form-control" name="api_secret">
	</div>
	
	<div class="input-group">
	  <span class="input-group-addon">Client ID </span>
	  <input type="text" class="form-control" name="client_id">
	</div>
	
	<div class="input-group">
	  <span class="input-group-addon">Client Secret </span>
	  <input type="text" class="form-control" name="client_secret">
	</div>
	
	<hr />
	
	<div class="input-group">
	  <span class="input-group-addon">Module Name </span>
	  <input type="text" class="form-control" name="module_name">
	</div>
	
	<div class="input-group">
	  <span class="input-group-addon">Module Name </span>
	  '.Form::select('module_name',$payFiles,old('module_name'),['class'=>'form-control']).'
	</div>
	

  </div>
  <!-- /.box-body -->

  <div class="box-footer">
	<button type="submit" class="btn btn-primary btn-success">'.trans('general.button.save').'</button>
	  '.csrf_field().'
  </div>
</form>
';

?>


@include('modal.modal',['title'=>trans('general.label.add_new').' | Payment Provider', 'body'=>$body])