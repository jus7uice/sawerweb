<?php 

$body = '
<form role="form" action="'.url(ADMIN_PATH.'sticker.stock.edit').'" method="post" id="ajxForm" enctype="multipart/form-data">
  <div class="box-body">
  
	<div class="panel panel-default">
		<div class="panel-body">
			<img src="'.url($data->media_url).'" heigh="100" />
		</div>
	</div>
	
	
	<div class="input-group">
	  <span class="input-group-addon">'.trans('general.label.title').' *</span>
	  <input type="text" class="form-control" name="title" value="'.$data->title.'" />
	</div>
	
	<div class="input-group">
	  <span class="input-group-addon">Coins Price *</span>
	  <input type="text" class="form-control" name="price" value="'.$data->price.'" />
	</div>
	
			
  </div>
  <!-- /.box-body -->

  <div class="box-footer">
	<button type="submit" class="btn btn-primary btn-success">'.trans('general.button.save').'</button>
	  '.Form::hidden('id',$data->id).'
	  '.csrf_field().'
  </div>
</form>
';

?>


@include('modal.modal',['title'=>trans('general.label.edit').' | Sticker Stock', 'body'=>$body])