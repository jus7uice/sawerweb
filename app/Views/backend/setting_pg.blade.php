@extends('layouts.backend')

{{-- Title --}}
@section('title')
	- Setting PG
@endsection

{{-- Page title --}}
@section('pagetitle')
	
@endsection

@section('content')
	
	<!-- Main content -->
    
      <div class="row">
        
		<div class="col-md-6">
			<div class="box box-primary">
				<form role="form" action="{{url(ADMIN_PATH.'setting.pg')}}" method="post" enctype="multipart/form-data">
					<div class="box-header with-border">
					  <h3 class="box-title">Setting PG</h3>
					</div>
					<div class="box-body">
					
						<div class="input-group">
						  <span class="input-group-addon">PG URL *</span>
						  <input type="text" class="form-control" name="pg_api_url" value="{{ $data['pg_api_url']??old('pg_api_url') }}" />
						</div>
						
						<hr />
						
						<div class="input-group">
						  <span class="input-group-addon">API Key *</span>
						  <input type="text" class="form-control" name="pg_api_key" value="{{ $data['pg_api_key']??old('pg_api_key') }}" />
						</div>
						
						<div class="input-group">
						  <span class="input-group-addon">API Secret *</span>
						  <input type="text" class="form-control" name="pg_api_secret" value="{{ $data['pg_api_secret']??old('pg_api_secret') }}" />
						</div>
						<hr />
						<div class="input-group">
						  <span class="input-group-addon">Client ID *</span>
						  <input type="text" class="form-control" name="pg_client_id" value="{{ $data['pg_client_id']??old('pg_client_id') }}" />
						</div>
						
						<div class="input-group">
						  <span class="input-group-addon">Client Secret *</span>
						  <input type="text" class="form-control" name="pg_client_secret" value="{{ $data['pg_client_secret']??old('pg_client_secret') }}" />
						</div>
										
					</div>
					 <div class="box-footer">
						<button type="submit" class="btn btn-success">{{trans('general.button.save')}}</button>
						 {{csrf_field()}}
					</div>
				</form>	
			</div>
		</div>
     
        
		<div class="col-md-6">
			<div class="box box-primary">
				<form role="form" action="{{url(ADMIN_PATH.'setting.pg.gettoken')}}" method="get" enctype="multipart/form-data">
					<div class="box-header with-border">
					  <h3 class="box-title">Generate Token</h3>
					</div>
					<div class="box-body">
					{{$data['pg_api_token']}}
						
					<p>&nbsp;</p>
					Expire In {{$data['pg_api_token_expired_at']}}
										
					</div>
					 <div class="box-footer">
						<button type="submit" class="btn btn-success">Generate New Token</button>
						 {{csrf_field()}}
					</div>
				</form>	
			</div>
		</div>
      </div>
      <!-- /.row -->

@endsection









