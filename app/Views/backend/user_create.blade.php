<?php 
$body = '
<form role="form" action="'.url(ADMIN_PATH.'user.create').'" method="post" id="ajxForm">
  <div class="box-body">
  
	<div class="input-group">
	 <span class="input-group-addon">'.trans('general.organizer.organizer').' *</span>
	 '.Form::select('organizer_id', $organizerList, null,['class'=>'form-control']).'
	</div>
	
	<div class="input-group">
	  <span class="input-group-addon">'.trans('general.label.name').' *</span>
	  <input type="text" class="form-control" name="name">
	</div>

	<div class="input-group">
	  <span class="input-group-addon">'.trans('general.people.email').' *</span>
	  <input type="email" class="form-control" name="email">
	</div>
			
	<div class="input-group">
	  <span class="input-group-addon">'.trans('general.people.password').' *</span>
	  <input type="password" class="form-control" name="password" value="secret">
	</div>
	<span class="help-block text-red">default password : "<strong>secret</strong>" *</span>
	
	<div class="form-group">
	   '.Form::hidden('status',0).'
		'.Form::checkbox('status',1,true).'
		 <label>'.trans('general.label.is_active').' *</label>
	</div>
	
  </div>
  <!-- /.box-body -->

  <div class="box-footer">
	<button type="submit" class="btn btn-primary btn-success">'.trans('general.button.save').'</button>
	  '.csrf_field().'
  </div>
</form>
';

?>


@include('modal.modal',['title'=>trans('general.label.add_new').' | '. trans('general.user.user'), 'body'=>$body])