<?php 
$body = '
<form role="form" action="'.url(ADMIN_PATH.'tag.edit').'" method="post" id="ajxForm">
  <div class="box-body">
	
	<div class="input-group">
	  <span class="input-group-addon">'.trans('general.label.name').' *</span>
	  <input type="text" class="form-control" name="name" value="'.$item->name.'">
	</div>
	
	<div class="form-group">
	   '.Form::hidden('is_trending',0).'
	   '.Form::checkbox('is_trending',1,($item->is_trending==1)?true:false).'
		 <label>'.trans('general.label.is_trending').' *</label>
	</div>
			
	<div class="form-group">
	   '.Form::hidden('status',0).'
		'.Form::checkbox('status',1,($item->status==1)?true:false).'
		 <label>'.trans('general.label.is_active').' *</label>
	</div>
			
  </div>
  <!-- /.box-body -->

  <div class="box-footer">
	<button type="submit" class="btn btn-primary btn-success">'.trans('general.button.save').'</button>
	  '.csrf_field().'
	  '.Form::hidden('id',request()->id).'
  </div>
</form>
';

?>


@include('modal.modal',['title'=>trans('general.label.edit').' | '. trans('general.tag.tag'), 'body'=>$body])