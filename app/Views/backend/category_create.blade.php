<?php 
$body = '
<form role="form" action="'.url(ADMIN_PATH.'category.create').'" method="post" id="ajxForm">
  <div class="box-body">
	
	<div class="input-group">
	  <span class="input-group-addon">'.trans('general.label.name').' *</span>
	  <input type="text" class="form-control" name="name">
	</div>
	
	<div class="input-group">
		 <span class="input-group-addon">'.trans('general.label.parent').'</span>
	   '.Form::select('parent_id', $categoryList, null,['class'=>'form-control']).'
	</div>
	
	<div class="form-group">
	   '.Form::hidden('is_top',0).'
	   '.Form::checkbox('is_top',1).'
		 <label>'.trans('general.label.is_top').' *</label>
	</div>
			
  </div>
  <!-- /.box-body -->

  <div class="box-footer">
	<button type="submit" class="btn btn-primary btn-success">'.trans('general.button.save').'</button>
	  '.csrf_field().'
  </div>
</form>
';

?>


@include('modal.modal',['title'=>trans('general.label.add_new').' | '. trans('general.category.category'), 'body'=>$body])