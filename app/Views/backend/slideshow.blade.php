@extends('layouts.backend')

{{-- Title --}}
@section('title')
	- {{trans('general.label.slideshow')}}
@endsection

{{-- Page title --}}
@section('pagetitle')
	{{trans('general.label.slideshow')}}
@endsection

@section('content')
	
	<!-- Main content -->
    
      <div class="row">
        
		<div class="col-md-6">		
			<p><a class="btn btn-primary" href="{{url(ADMIN_PATH.'slideshow.create')}}" data-toggle="ajaxModal" >{{trans('general.label.add_new')}}</a></p>
        </div>
		
		<div class="col-md-12">
			<div class="box box-primary">
				<form role="form" action="{{url(ADMIN_PATH.'slideshow.delete')}}" method="post" id="ajxFormDelete">
				<div class="box-header with-border">
				  <h3 class="box-title">{{trans('general.label.slideshow')}}</h3>
				</div>
				<div class="box-body">
				
					<table id="dataTbl" class="table table-bordered table-hover datatable" data-ajax="{{url(ADMIN_PATH.'slideshow.data')}}" data-processing="true" data-server-side="true"  data-length-menu="[50,100,250]">
						<thead>
						<tr>
						  <th data-data="chkbox" data-orderable="false" width="50"><input type="checkbox" id="select-all" /></th>
						  <th data-data="thumbnail" data-orderable="false" width="50">{{trans('general.media.thumb')}}</th>
						  <th data-data="lbl_detail" >{{trans('general.label.title')}}</th>
						  <th data-data="url">{{trans('general.label.url')}}</th>
						  <th data-data="ordered_num" width="50">{{trans('general.label.ordered_num')}}</th>
						  <th data-data="status" data-search="false">{{trans('general.label.status')}}</th>
						  <th data-data="action" width="50"></th>
						</tr>
						</thead>
						
					</table>
				
				</div>
				 <div class="box-footer">
					<button type="submit" class="btn btn-danger">{{trans('general.button.delete')}}</button>
					 {{csrf_field()}}
				</div>
				</form>	
			</div>
		</div>
      </div>
      <!-- /.row -->

@endsection

@section('css')
<link rel="stylesheet" href="{{asset('plugins/fancybox/jquery.fancybox.css')}}" type="text/css">
@endsection
@section('js')
<script src="{{asset('plugins/fancybox/jquery.fancybox.js')}}"></script>
<script>
$(function(){
	// $(".fancybox").fancybox();
	$(document).on('click', '.fancybox',function(e) { 
		e.preventDefault();
		$(this).fancybox(); 
		return false; 
	});
});
</script>

@endsection