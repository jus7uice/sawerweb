<ul class="mailbox-attachments clearfix">
@foreach($data as $row)
	<li>
		<a class="fancybox mailbox-attachment-name" href="{{url($row->path.$row->bigimg)}}" rel="gallery_manager">
		<span class="mailbox-attachment-icon has-img">
			<img src="{{url($row->path.$row->thumb)}}" alt="{{$row->title}}" />
		</span>
		</a>
		<div class="mailbox-attachment-info">
			<a class="fancybox mailbox-attachment-name" href="{{url($row->path.$row->bigimg)}}"  rel="gallery_manager2">{{truncate($row->title)}}</a>
			<span class="mailbox-attachment-size">{{$row->created_at}}
			<a href="{{url(ADMIN_PATH.'media.delete?id='.$row->id)}}" onclick="return confirm('Are you sure to delete\n{{truncate($row->title)}}\n?')" class="btn btn-default btn-xs pull-right"><i class="fa fa-trash"></i></a>
			</span>
		</div>
	</li>	

@endforeach
</ul>

{{ $data->links() }}
