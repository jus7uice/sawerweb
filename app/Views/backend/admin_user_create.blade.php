<?php 
$body = '
<form role="form" action="'.url(ADMIN_PATH.'admin.user.create').'" method="post" id="ajxForm">
  <div class="box-body">
	
	<div class="input-group">
		 <span class="input-group-addon">'.trans('general.people.group').' *</span>
	   '.Form::select('group', $groupList, null,['class'=>'form-control']).'
	</div>
	
	<div class="input-group">
	  <span class="input-group-addon">'.trans('general.people.username').' *</span>
	  <input type="text" class="form-control" name="username">
	</div>
	
	<div class="input-group">
	  <span class="input-group-addon">'.trans('general.people.password').' *</span>
	  <input type="password" class="form-control" name="password">
	</div>
	
	<br />
	
	<div class="input-group">
	  <span class="input-group-addon">'.trans('general.people.name').' *</span>
	  <input type="text" class="form-control" name="name">
	</div>
	
	<div class="input-group">
	  <span class="input-group-addon">'.trans('general.people.email').' *</span>
	  <input type="email" class="form-control" name="email">
	</div>
	
  </div>
  <!-- /.box-body -->

  <div class="box-footer">
	<button type="submit" class="btn btn-primary btn-success">'.trans('general.button.save').'</button>
	  '.csrf_field().'
  </div>
</form>
';

?>


@include('modal.modal',['title'=>trans('general.label.add_new'), 'body'=>$body])