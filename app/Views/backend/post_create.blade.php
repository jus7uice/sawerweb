@extends('layouts.backend')

{{-- Title --}}
@section('title')
	- {{trans('general.label.add_new')}} | {{trans('general.post.post')}}
@endsection

{{-- Page title --}}
@section('pagetitle')
	
@endsection

@section('content')
	
	<!-- Main content -->
    
      <div class="row">
        
		<div class="col-md-8 col-md-offset-2">
			<div class="box box-primary">
				<form role="form" action="{{url(ADMIN_PATH.'post.create')}}" method="post" enctype="multipart/form-data">
					<div class="box-header with-border">
					  <h3 class="box-title">{{trans('general.label.add_new')}} | {{trans('general.post.post')}}</h3>
					</div>
					<div class="box-body">
						
						<div class="input-group">
							 <span class="input-group-addon">{{trans('general.category.category')}} *</span>
							 {{Form::select('category_id', $categoryList, null,['class'=>'form-control'])}}
						</div>
					
						<div class="input-group">
						  <span class="input-group-addon">{{trans('general.post.title')}} *</span>
						  <input type="text" class="form-control" name="title" value="{{ old('title') }}" />
						</div>
						
						<div class="input-group">
						  <span class="input-group-addon">Video URL *</span>
						  <input type="text" class="form-control" name="video_url" value="{{ old('video_url') }}" />
						</div>
						
						<div class="form-group">
							<label>{{trans('general.label.description')}}</label>
							 <div class="form-line">
								  <textarea id="" class="editor form-control" name="description" rows="5">{{ old('description') }}</textarea>
							 </div>
						</div>
						
						<div class="form-group input-group">
							<span class="input-group-addon">{{trans('general.tag.tag')}}</span>
							<select class="form-control tag" multiple="multiple" data-placeholder="Select #tag" name="tag[]"></select>
						  
						</div>
					
					</div>
					 <div class="box-footer">
						<button type="submit" class="btn btn-success">{{trans('general.button.save')}}</button>
						 {{csrf_field()}}
					</div>
				</form>	
			</div>
		</div>
      </div>
      <!-- /.row -->

@endsection

 
@section('css')
<link rel="stylesheet" href="{{asset('adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}" type="text/css">
<!-- Select2 -->
<link rel="stylesheet" href="{{asset('adminlte/bower_components/select2/dist/css/select2.min.css')}}" type="text/css">
@stop
@section('js')
<!-- Rdc -->
<script src="{{asset('adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>

<!-- Select2 -->
<script src="{{asset('adminlte/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
<script>
$(function(){
	/* wysiwyg */
	$('.editor').wysihtml5({
		toolbar: {
        'font-styles': true,
        'color': false,
        
        'blockquote': false,
        'lists': false,
        'html': false,
        'link': true,
        'image': false,
        'smallmodals': false
      }
	});
	
	//Initialize Select2 Elements
    $('.tag').select2({
		ajax: {
			minimumInputLength: 2,
			url: "{{url(ADMIN_PATH.'tag.json')}}",
			dataType: 'json',
			delay: 250,
			processResults: function (data) {
				return {results: data };
			},
			cache: true
		}
	});
	
	$('.tag').select2('val', ["1","2"], true);
	
});
</script>

@stop








