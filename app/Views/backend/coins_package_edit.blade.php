<?php 
$body = '
<form role="form" action="'.url(ADMIN_PATH.'coins.package.edit').'" method="post" id="ajxForm">
  <div class="box-body">
	
	<div class="input-group">
	  <span class="input-group-addon">'.trans('general.label.title').' *</span>
	  <input type="text" class="form-control" name="title" value="'.$data->title.'">
	</div>
	
	<div class="input-group">
	  <span class="input-group-addon">Price *</span>
	  <input type="text" class="form-control" name="price" value="'.$data->price.'">
	</div>
	
	<div class="input-group">
	  <span class="input-group-addon">Total Coins *</span>
	  <input type="text" class="form-control" name="total_coins" value="'.$data->total_coins.'">
	</div>
	
	<div class="form-group">
	   '.Form::hidden('is_fav',0).'
	   	'.Form::checkbox('is_fav',1,($data->is_fav==1)?true:false).'
		 <label>Is Favorite? *</label>
	</div>
			
  </div>
  <!-- /.box-body -->

  <div class="box-footer">
	<button type="submit" class="btn btn-primary btn-success">'.trans('general.button.save').'</button>
		'.Form::hidden('id',$data->id).'
	  '.csrf_field().'
  </div>
</form>
';

?>


@include('modal.modal',['title'=>trans('general.label.edit').' | Coins Package', 'body'=>$body])