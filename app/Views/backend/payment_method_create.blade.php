<?php 
$body = '
<form role="form" action="'.url(ADMIN_PATH.'payment.method.create').'" method="post" id="ajxForm">
  <div class="box-body">
	
	<div class="input-group">
		 <span class="input-group-addon">Payment Provider *</span>
		 '.Form::select("payment_provider_id", $providerList, null,['class'=>'form-control']).'
	</div>
	
	<div class="input-group">
	  <span class="input-group-addon">'.trans('general.label.name').' *</span>
	  <input type="text" class="form-control" name="name">
	</div>
	
	<div class="input-group">
	  <span class="input-group-addon">Code *</span>
	  <input type="text" class="form-control" name="code">
	</div>
	
	<div class="input-group">
	  <span class="input-group-addon">Logo Url </span>
	  <input type="text" class="form-control" name="logo_url">
	</div>
	


  </div>
  <!-- /.box-body -->

  <div class="box-footer">
	<button type="submit" class="btn btn-primary btn-success">'.trans('general.button.save').'</button>
	  '.csrf_field().'
  </div>
</form>
';

?>


@include('modal.modal',['title'=>trans('general.label.add_new').' | Payment Method', 'body'=>$body])