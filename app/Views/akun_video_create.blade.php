@extends('layouts.web')

@section('page_title')
Koleksi Video / Tambah
@endsection

@section('content')
<!-- First Container -->
<div class="container-fluid bg-1">
	<h3 class="margin text-center">Koleksi Video </h3>



	<div class="container">
		<form action="{{url('/akun/video/tambah')}}" method="post" enctype="multipart/form-data">
			{{csrf_field()}}  
			<div class="row row-no-gutter">
				
				<div class="col-md-10">				
								   
					<div class="form-group">
						<label for="">Kategori</label>
						{{Form::select('category_id',$categoryList, null,['class'=>'form-control'])}}
					</div>
					<div class="form-group">
						<label for="">Judul *</label>
						<input type="text" class="form-control" name="title" placeholder="Judul" value="{{old('title')}}">
					</div>
					
					<div class="form-group">
						<label>URL Video *</label>
						<input type="text" class="form-control" name="video_url" placeholder="Contoh: https://youtube/abcdef"  value="{{old('video_url')}}">						
					</div>
					
					<div class="form-group">
						<label>Deskripsi </label>
						<textarea class="form-control" name="description" rows="3">{{old('description')}}</textarea>						
					</div>
					
					
						
				</div>
				
				
				
			</div>
			<div class="row">
				<div class="col-md-12 text-center">
				<hr />
					<button type="submit" class="btn btn-primary">Simpan</button>			
				</div>
			</div>
		</form>
	</div>
							
  
</div>

@endsection

@section('css')
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/4.0.0/css/jasny-bootstrap.min.css">
<!-- Latest compiled and minified JavaScript -->
<script src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/4.0.0/js/jasny-bootstrap.min.js"></script>
<style>
.form-group label {font-weight: bold;}

</style>
@endsection