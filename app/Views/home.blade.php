@extends('layouts.web')

@section('page_title')
Home
@endsection

@section('content')
<!-- First Container -->
<div class="container">

	{{--debug(Request()->get('auth_user'))--}}

	<div class="row">  
		<div class="col-sm-12">
			<p><marquee scrollamount="10">
			<strong>People</strong> This is basic example of marquee &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<strong>People</strong> This is basic example of marquee &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<strong>People</strong> This is basic example of marquee &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			
			</marquee></p>
		</div>
	</div>
	

	<div class="row">  

		<div class="col-sm-9">
			<h3 class="margin">Terbaru</h3>  
			@foreach($video as $row)
			
			
			<div class="col-xl-12 col-sm-3">
				<a href="{{url('/video/'.$row->hashed_id)}}">
					<div class="panel panel-default">
					  
						<div class="embed-container">
						<img src="{{ LaravelVideoEmbed::getYoutubeThumbnail($row->video_url) }}" class="">
						</div>
					</div>
				</a>
				
			</div>
			@endforeach
		</div>
		
		<div class="col-sm-3">
			<h3 class="margin">Creator Populer</h3>  
			
			<ul class="list-group">
				@foreach($video as $row)
                <li class="list-group-item">
                  <span class="badge">14 fans</span>
				   <div class="square-box pull-left">
                        <img src="{{ LaravelVideoEmbed::getYoutubeThumbnail($row->video_url) }}" height="20" class="">
                    </div>
					
					Fredy
                </li>
				@endforeach
				
			</ul>
		
		</div>


	<?php /*
	<div class="col-xl-12 col-sm-12">
	<div class="slickme">
	  
		@foreach($video as $row)
		<div class="slick-slide" style="width: 400px; margin: 10px 20px; border: 1px solid #fafafa;">
			<div class="embed-container">
			<img src="{{ LaravelVideoEmbed::getYoutubeThumbnail($row->video_url) }}" class="">
			</div>
			
			
			APA
		</div>
		@endforeach
	  
	</div>
	</div>
		
	@foreach($video as $row)
	<!-- video-testimonail -->
	<div class="col-xl-3 col-lg-3 col-md-3 col-sm-12">
	<div class="video-testimonial-block">
		<!--<div class="video-thumbnail"><img src="https://easetemplate.com/free-website-templates/finvisor/images/testi_img_1.jpg" alt="" class="img-fluid"></div>-->
		<div class="embed-container">
			<iframe src="https://www.youtube.com/embed/KzhnnAcXmro" allowfullscreen></iframe>
		</div>
		<a href="#" class="video-play"></a>
	</div>
	<div class="video-testimonial-content">
		<h4 class="mb10">Harlan M. Williams</h4>
		<p>Student Loan</p>
	</div>
	</div>
	<!-- /.video-testimonail -->
	@endforeach
	*/ ?>

	</div>
</div>
  
  
<!-- Second Container -->
<div class="container-fluid bg-2 text-center">
	<h3 class="margin">POPULER</h3>
  
	<div class="container">
	  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
	  <a href="#" class="btn btn-default btn-lg">
		<span class="glyphicon glyphicon-search"></span> Search
	  </a>
	</div>
</div>

<!-- Third Container (Grid) -->
<div class="container-fluid bg-3 text-center">    
  <h3 class="margin">Where To Find Me?</h3><br>
  <div class="row">
    <div class="col-sm-4">
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
	  </div>
     
    <div class="col-sm-4"> 
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
    </div>
    <div class="col-sm-4"> 
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
    </div>
	</div>
</div>

@endsection

@section('js')
  <script>
  $(function(){
	 $('.slickme').slick({
		lazyLoad: 'ondemand',
		// dots: true,
		infinite: true,
		// speed: 300,
		slidesToShow: 4,
		slidesToScroll: 4,
		centerMode: true,
		// variableWidth: true
	 }); 
  });
  </script>
@endsection

@section('css')
<style>
	.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
	.embed-container iframe, .embed-container object, .embed-container embed, .embed-container img { position: absolute; top: 0; left: 0; width: 100%; height: 100%;}  
  
	.video-testimonial-block { position: relative; width: auto; height: 206px; overflow: hidden; margin-bottom: 30px; }
	.video-testimonial-block .video-thumbnail { height: 100%; width: 100%; position: absolute; z-index: 1; background-size: cover; top: 0; left: 0; }
	.video-testimonial-block .video { }
	.video-testimonial-block .video iframe { position: absolute; top: 0; left: 0; width: 100%; height: 100%; border: 0px; }
	.video-testimonial-block .video-play { position: absolute; z-index: 2; top: 50%; left: 50%; margin-left: -40px; margin-top: -18px; text-decoration: none; }
	.video-testimonial-block .video-play::before { content: "\f144"; font: normal normal normal 14px/1; font-family: 'Font Awesome\ 5 Free'; font-weight: 900; font-size: inherit; text-rendering: auto; -webkit-font-smoothing: antialiased; -moz-osx-font-smoothing: grayscale; font-size: 50px; color: #b3b5bc; }
	.video-testimonial-block .video-play:hover::before { color: #172651; }
</style>
@endsection
