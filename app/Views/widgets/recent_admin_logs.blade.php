<!-- PRODUCT LIST -->
<div class="box box-primary">
	<div class="box-header with-border">
	  <h3 class="box-title">Recently Activities</h3>

	  <div class="box-tools pull-right">
		<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
		</button>
		<!-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button> -->
	  </div>
	</div>
	<!-- /.box-header -->
	<div class="box-body">
	  <ul class="products-list product-list-in-box">
		
		@foreach($data as $item)
		<li class="item">
		 <div class="product-infoXXX">
				<span class="product-description">
				{{$item->created_at}} -{{$item->name}} : ({{$item->method}}) {{$item->description}}
				</span>
		  </div>
		</li>	
		<!-- /.item -->
		@endforeach
		
	  </ul>
	</div>
	<!-- /.box-body -->

	<div class="box-footer text-center">
	  <a href="{{url(ADMIN_PATH.'admin.log')}}" class="uppercase">{{trans('general.label.view_all')}} {{trans('general.admin.logs')}}</a>
	</div>
	<!-- /.box-footer -->
</div>
<!-- /.box -->