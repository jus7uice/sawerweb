@extends('layouts.web')

@section('page_title')
Login
@endsection

@section('content')
<!-- First Container -->
<div class="container text-center">
	
	<h2>Aktivasi Akun</h2>
	
	@if($data)	
		@if($data->email_verified_at != "")	
			<div class="alert alert-success">Akun telah aktif :) </div>
		@else
			<div class="alert alert-success">Selamat! Aktivasi akun berhasil :) </div>
		@endif
	@else
		<div class="alert alert-danger">Aktivasi gagal. Silahkan periksa kembali kotak email Anda</div>
	@endif
</div>

@endsection