@extends('layouts.web')

@section('page_title')
Profil
@endsection

@section('content')
<!-- First Container -->
<div class="container-fluid bg-1">
	<h3 class="margin text-center">Ubah Profil</h3>



	<div class="container">
		<form action="{{url('/akun/profil/ubah')}}" method="post" enctype="multipart/form-data">
			{{csrf_field()}}  
			<div class="row row-no-gutter">
				<div class="col-md-2">
					
					<div class="fileinput fileinput-new" data-provides="fileinput">
					  <div class="fileinput-new img-thumbnail" style="width: 100%; max-width: 200px; height: 150px;">
						<img src="{{($user->photo_small_url!="")?url($user->photo_small_url):'https://dummyimage.com/200x160/f2ff00/7c8244.jpg&text='.make_initials($user->name)}}"  alt="...">
					  </div>
					  <div class="fileinput-preview fileinput-exists img-thumbnail" style="max-width: 200px; max-height: 150px;"></div>
					  <div>
						<span class="btn btn-outline-secondary btn-file btn-sm"><span class="fileinput-new"><i class="fa fa-upload"></i> &nbsp; Foto Profil </span><span class="fileinput-exists">Ganti</span>
						<input type="file" name="file"></span>
						<a href="#" class="btn btn-outline-secondary btn-sm fileinput-exists" data-dismiss="fileinput">Buang</a>
					  </div>
					</div>
					{{Form::hidden('photo',$user->photo_big_url)}}
				</div>
				<div class="col-md-5">				
								   
					<div class="form-group">
						<label for="">Email</label>
						<input type="email" class="form-control" placeholder="Email" value="{{$user->email??old('email')}}" disabled>
					</div>
					<div class="form-group">
						<label for="">Nama Lengkap *</label>
						<input type="text" class="form-control" name="name" placeholder="Nama" value="{{$user->name??old('name')}}">
					</div>
					
					<div class="form-group">
						<label>Username *</label>
						<div class="input-group">
							<div class="input-group-addon">{{URL('/')}}/</div>
							<input type="text" class="form-control" name="username" placeholder="Nama Unik"  value="{{$user->username??old('username')}}">
						</div>
					</div>
					
					<div class="form-group">
						<label>Tentang kamu </label>
						<textarea class="form-control" name="about_me" rows="3">{{$user->about_me??old('about_me')}}</textarea>
						
					</div>
					
					<div class="form-group row-no-gutters">
					  <label class="col-lg-4 control-label pull-left">Gender *</label>
					  <div class="col-lg-8">
						<div class="radio">
						  <label>
							<input type="radio" name="gender" id="optionsRadios1" value="L" {{($user->gender=='L')?"checked":""}} >
							Laki-laki
						  </label>
						</div>
						<div class="radio">
						  <label>
							<input type="radio" name="gender" id="optionsRadios2" value="P" {{($user->gender=='P')?"checked":""}}>
							Perempuan
						  </label>
						</div>
					  </div>
					</div>
						
				</div>
				
				<div class="col-md-5">				
								   
					<div class="form-group">
						<label for="">Laman Website</label>
						<input type="text" class="form-control" name="web_url" placeholder="http://" value="{{$user->web_url??old('web_url')}}">
					</div>
					
								   
					<div class="form-group">
						<label for="">Laman Youtube</label>
						<input type="text" class="form-control" name="yt_url" placeholder="http://" value="{{$user->yt_url??old('yt_url')}}">
					</div>
					
								   
					<div class="form-group">
						<label for="">Laman Facebook</label>
						<input type="text" class="form-control" name="fb_url" placeholder="" value="{{$user->fb_url??old('fb_url')}}">
					</div>
					
								   
					<div class="form-group">
						<label for="" class="">Laman / Akun Instagram </label>
						<input type="text" class="form-control" name="ig_url" placeholder="" value="{{$user->ig_url??old('ig_url')}}">
					</div>
								   
					<div class="form-group">
						<label for="" class="">Laman / Akun Twitter </label>
						<input type="text" class="form-control" name="tw_url" placeholder="" value="{{$user->tw_url??old('tw_url')}}">
					</div>
					
					
					
					<br />
										
										
				</div>
				
				
			</div>
			<div class="row">
				<div class="col-md-12 text-center">
				<hr />
					<button type="submit" class="btn btn-primary">Simpan Perubahan</button>			
				</div>
			</div>
		</form>
	</div>
							
  
</div>

@endsection

@section('css')
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/4.0.0/css/jasny-bootstrap.min.css">
<!-- Latest compiled and minified JavaScript -->
<script src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/4.0.0/js/jasny-bootstrap.min.js"></script>
<style>
.form-group label {font-weight: bold;}

</style>
@endsection