<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.0-alpha1/css/bootstrap.min.css" rel="stylesheet">
		
		<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
		<!------ Include the above in your HEAD tag ---------->
		
		<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" type="text/css" rel="stylesheet" />

		

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

           .container{max-width:1170px; margin:auto;}
			img{ max-width:100%;}
			.inbox_people {
			  background: #f8f8f8 none repeat scroll 0 0;
			  float: left;
			  overflow: hidden;
			  width: 40%; border-right:1px solid #c4c4c4;
			}
			.inbox_msg {
			  border: 1px solid #c4c4c4;
			  clear: both;
			  overflow: hidden;
			}
			.top_spac{ margin: 20px 0 0;}


			.recent_heading {float: left; width:40%;}
			.srch_bar {
			  display: inline-block;
			  text-align: right;
			  width: 60%; padding:
			}
			.headind_srch{ padding:10px 29px 10px 20px; overflow:hidden; border-bottom:1px solid #c4c4c4;}

			.recent_heading h4 {
			  color: #05728f;
			  font-size: 21px;
			  margin: auto;
			}
			.srch_bar input{ border:1px solid #cdcdcd; border-width:0 0 1px 0; width:80%; padding:2px 0 4px 6px; background:none;}
			.srch_bar .input-group-addon button {
			  background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
			  border: medium none;
			  padding: 0;
			  color: #707070;
			  font-size: 18px;
			}
			.srch_bar .input-group-addon { margin: 0 0 0 -27px;}

			.chat_ib h5{ font-size:15px; color:#464646; margin:0 0 8px 0;}
			.chat_ib h5 span{ font-size:13px; float:right;}
			.chat_ib p{ font-size:14px; color:#989898; margin:auto}
			.chat_img {
			  float: left;
			  width: 11%;
			}
			.chat_ib {
			  float: left;
			  padding: 0 0 0 15px;
			  width: 88%;
			}

			.chat_people{ overflow:hidden; clear:both;}
			.chat_list {
			  border-bottom: 1px solid #c4c4c4;
			  margin: 0;
			  padding: 18px 16px 10px;
			}
			.inbox_chat { height: 550px; overflow-y: scroll;}

			.active_chat{ background:#ebebeb;}

			.incoming_msg_img {
			  display: inline-block;
			  width: 6%;
			}
			.received_msg {
			  display: inline-block;
			  padding: 0 0 0 10px;
			  vertical-align: top;
			  width: 92%;
			 }
			 .received_withd_msg p {
			  background: #ebebeb none repeat scroll 0 0;
			  border-radius: 3px;
			  color: #646464;
			  font-size: 14px;
			  margin: 0;
			  padding: 5px 10px 5px 12px;
			  width: 100%;
			}
			.time_date {
			  color: #747474;
			  display: block;
			  font-size: 12px;
			  margin: 8px 0 0;
			}
			.received_withd_msg { width: 80%;}
			.mesgs {
			  float: left;
			  padding: 30px 15px 0 25px;
			  width: 100%;
			}

			 .sent_msg p {
			  background: #05728f none repeat scroll 0 0;
			  border-radius: 3px;
			  font-size: 14px;
			  margin: 0; color:#fff;
			  padding: 5px 10px 5px 12px;
			  width:100%;
			}
			.outgoing_msg{ overflow:hidden; margin:26px 0 26px;}
			.sent_msg {
			  float: right;
			  width: 80%;
			}
			.input_msg_write input {
			  background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
			  border: medium none;
			  color: #4c4c4c;
			  font-size: 15px;
			  min-height: 48px;
			  width: 100%;
			}

			.type_msg {border-top: 1px solid #c4c4c4;position: relative;}
			.msg_send_btn {
			  background: #05728f none repeat scroll 0 0;
			  border: medium none;
			  border-radius: 50%;
			  color: #fff;
			  cursor: pointer;
			  font-size: 17px;
			  height: 33px;
			  position: absolute;
			  right: 0;
			  top: 11px;
			  width: 33px;
			}
			.messaging { padding: 0 0 50px 0;}
			.msg_history {
			  height: 516px;
			  overflow-y: auto;
			}
	
	
			.sidebar
			{
				background-color:orange;
				height:250px;
				padding:15px;
			}
			.stick {
				position: fixed;
				width: 26.7%;
				top: 00px;
			}
	
			.abs {
				position: absolute;
				bottom: 0px;
				width: 92.5%;
			}
	
			
		</style>
		
		
    </head>
    <body>
	
	

	
	
	
	
	
	
	
   
				<div class="container">
					<h1 class="display-3">Display 3</h1>
					<div class="row">
						<div class="col-8 left-container">
						
							<center>
							
									<iframe src="https://www.youtube.com/embed/Z6Jr0aBcz4U?wmode=transparent&amp;autoplay=1&amp;loop=1&amp;controls=1" type="" width="100%" height="443" frameborder="0" allowfullscreen class="iframe-class" data-html5-parameter></iframe>
								
							</center>
							
							<p class="lead">
								hi guys support gw ya... Thank buat gift kalian 
							</p>
							
						
						
						
							<p>
									Penonton : 1504
							</p>
						
						
						
							<!-- Button trigger modal -->
							<center>
							<button type="button" class="btn btn-lg btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
							  Berikan hadiah
							</button>
							</center>

							<!-- Modal -->
							<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
							  <div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
								  <!--
								  <div class="modal-header">
									<h5 class="modal-title" id="exampleModalLongTitle">Berikan support terbaik mu</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									  <span aria-hidden="true">&times;</span>
									</button>
								  </div>
								  -->
								  <div class="modal-body">
									
										<fieldset class="form-group">
											<div class="row">
											  <legend class="col-form-label col-sm-2 pt-0"></legend>
											  <div class="col-sm-10">
												
												<div class="form-check">
												  <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios1" value="option1" checked>
												  <label class="form-check-label" for="gridRadios1">
													1000
												  </label>
												</div>	
												
												<div class="form-check">
												  <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios1" value="option1" >
												  <label class="form-check-label" for="gridRadios1">
													5000
												  </label>
												</div>															
												
												<div class="form-check">
												  <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios1" value="option1" >
												  <label class="form-check-label" for="gridRadios1">
													10000
												  </label>
												</div>															
												
												<div class="form-check">
												  <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios1" value="option1" >
												  <label class="form-check-label" for="gridRadios1">
													25000
												  </label>
												</div>															
												
												<div class="form-check">
												  <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios1" value="option1" >
												  <label class="form-check-label" for="gridRadios1">
													50000
												  </label>
												</div>															
												
												<div class="form-check">
												  <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios1" value="option1" >
												  <label class="form-check-label" for="gridRadios1">
													100000
												  </label>
												</div>															
												
											  </div>
											</div>
										  </fieldset>
									
									
								  </div>
								  <div class="modal-footer">
									<!--<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> -->
									<button type="button" class="btn btn-primary">Kirim hadiah..</button>
								  </div>
								</div>
							  </div>
							</div>
						
							<iframe src="http://chat.dev/chat?session_id=abc1234&nickname=fredy&user_profile_url=http://google.com/&signature=c4ca4238a0b923820dcc509a6f75849b" width="100%" height="600"></iframe>
						
							<p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
						
						
						
						
						
						</div>
						
						<div class="col-4 roght-container">
						
						
						
													
								<div id="sticky-anchor"></div>
									<div class="sidebar">
									
									<p>
									lorem ipsum is a free text , consectetur adipisicing elit, sed do eiusmod
									tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
									quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
									consequat. Duis aute irure dolor

								</p>

								<p>
									lorem ipsum is a free text , consectetur adipisicing elit, sed do eiusmod
									tempor incididunt

								</p>

										

									</div><!-- sidebar ends -->
						
						
						
						
							
							
								<div class="messaging">
								
								 <div class="inbox_msg">
																		
									<div class="mesgs">
									  <div class="msg_history">
										<div class="incoming_msg">
										  <div class="incoming_msg_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>
										  <div class="received_msg">
											<div class="received_withd_msg">
											  <p>Test which is a new approach to have all
												solutions</p>
											  <span class="time_date"> 11:01 AM    |    June 9</span></div>
										  </div>
										</div>
										
								
										<div class="incoming_msg">
										  <div class="incoming_msg_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>
										  <div class="received_msg">
											<div class="received_withd_msg">
											  <p>Test, which is a new approach to have</p>
											  <span class="time_date"> 11:01 AM    |    Yesterday</span></div>
										  </div>
										</div>
										
										<div class="incoming_msg">
										  <div class="incoming_msg_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>
										  <div class="received_msg">
											<div class="received_withd_msg">
											  <p>We work directly with our designers and suppliers,
												and sell direct to you, which means quality, exclusive
												products, at a price anyone can afford.</p>
											  <span class="time_date"> 11:01 AM    |    Today</span></div>
										  </div>
										</div>
										
										<div class="outgoing_msg">
										  <div class="sent_msg">
											<p>Test which is a new approach to have all
											  solutions</p>
											<span class="time_date"> 11:01 AM    |    June 9</span> </div>
										</div>
										<div class="incoming_msg">
										  <div class="incoming_msg_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>
										  <div class="received_msg">
											<div class="received_withd_msg">
											  <p>Test, which is a new approach to have</p>
											  <span class="time_date"> 11:01 AM    |    Yesterday</span></div>
										  </div>
										</div>
										
										<div class="incoming_msg">
										  <div class="incoming_msg_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>
										  <div class="received_msg">
											<div class="received_withd_msg">
											  <p>We work directly with our designers and suppliers,
												and sell direct to you, which means quality, exclusive
												products, at a price anyone can afford.</p>
											  <span class="time_date"> 11:01 AM    |    Today</span></div>
										  </div>
										</div>
										
										
										
									  </div>
									  <div class="type_msg">
										<div class="input_msg_write">
										  <input type="text" class="write_msg" placeholder="Type a message" />
										  <button class="msg_send_btn" type="button"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
										</div>
									  </div>
									</div>
								  </div>
								</div>
						
						
						
						</div>
					</div>
					
							
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
				</div>
        
    </body>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.0-alpha1/js/bootstrap.min.js"></script>
	<script>
		function sticky_relocate() {
			var window_top = $(window).scrollTop() ;
			// var footer_top = $(".footer").offset().top - 30;
			var div_top = $('#sticky-anchor').offset().top;
			var div_height = $(".sidebar").height();
			var leftHeight = $('.left-container').height(); 

			// if (window_top + div_height > footer_top){
				// $('.sidebar').removeClass('stick');
				// $('.sidebar').addClass('abs');
				 // $('.right-container').css('min-height', leftHeight + 'px');
				// }
			// else 
				
			if (window_top > div_top) {
				$('.sidebar').addClass('stick');
				$('.sidebar').removeClass('abs');
			} else {
				$('.sidebar').removeClass('stick');
				$('.sidebar').removeClass('abs');
			}
		}

		$(function () {
			$(window).scroll(sticky_relocate);
			sticky_relocate();
		});

	
	</script>
	
	


</html>
