@extends('layouts.web')

@section('page_title')
Login
@endsection

@section('content')
<!-- First Container -->
<div class="container">
  
	
    <p class="login-box-msg">Mendaftar</p>

	 <form action="{{url('register')}}" method="post">
		<div class="form-group">
			<label for="exampleInputEmail1">Email *</label>
			<input type="email" class="form-control" name="email" placeholder="Email" value="{{old('email')}}">
		</div>
		<div class="form-group">
			<label for="exampleInputEmail1">Nama Lengkap *</label>
			<input type="text" class="form-control" name="name" placeholder="Nama" value="{{old('name')}}">
		</div>
		
		<div class="form-group">
			<label>Username *</label>
			<div class="input-group">
				<div class="input-group-addon">{{URL('/')}}/</div>
				<input type="text" class="form-control" name="username" placeholder="username"  value="{{old('username')}}">
			</div>
		</div>
		
		
		<div class="form-group">
			<label for="exampleInputPassword1">Password *</label>
			<input type="password" class="form-control" placeholder="Password" name="password" />
		</div>
		
		
		<div class="form-group">
			<label for="exampleInputPassword1">Konfirmasi Password</label>
			<input type="password"  id="password-confirm"  class="form-control" placeholder="Konfirmasi Password" name="password_confirmation">
		</div>
		
		{{csrf_field()}}
		<button type="submit" class="btn btn-default">Daftar</button>
	</form>
	
	
	
	
</div>

@endsection