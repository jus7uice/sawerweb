@extends('layouts.web')

@section('page_title')
Home
@endsection

@section('content')
<!-- First Container -->
<div class="container">

	<div class="row">  
		
		<div class="col-sm-12 text-center">
			{{ $data->links() }}
		</div>
		
		<div class="col-sm-12">
			<div class="row">
		
			@foreach($data as $row)
			
			@php
				$color = ['bg-yellow','bg-blue','bg-aqua','bg-green','bg-red'];			
			@endphp
			
			<div class="col-md-3">
				<a href="{{url($row->username)}}">
			  <div class="box box-widget widget-user">
				<div class="widget-user-header {{$color[rand(0,4)]}}">
				  <h3 class="widget-user-username">{{$row->name}}</h3>
				  <h5 class="widget-user-desc">{{$row->aboutme}}</h5>
				</div>
				<div class="widget-user-image">
				  <img class="img-circle" src="https://bootdey.com/img/Content/avatar/avatar1.png" alt="User Avatar">
				</div>
				<div class="box-footer">
				  <div class="row">
					<div class="col-sm-4 border-right">
					  <div class="description-block">
						<h5 class="description-header">3,200</h5>
						<span class="description-text">VIEWER</span>
					  </div>
					</div>
					<div class="col-sm-4 border-right">
					  <div class="description-block">
						<h5 class="description-header">13,000</h5>
						<span class="description-text">FOLLOWERS</span>
					  </div>
					</div>
					<div class="col-sm-4">
					  <div class="description-block">
						<h5 class="description-header">35</h5>
						<span class="description-text">VIDEO</span>
					  </div>
					</div>
				  </div>
				</div>
			  </div>
			</div>
			
			@endforeach
			</div>
		</div>
		<div class="col-sm-12 text-center">
			{{ $data->links() }}
		</div>
	
	</div>
</div>

@endsection

@section('css')
<style>
	.box-widget {
    border: none;
    position: relative;
}
.box {
    position: relative;
    border-radius: 3px;
    background: #ffffff;
    border-top: 3px solid #d2d6de;
    margin-bottom: 20px;
    width: 100%;
    box-shadow: 0 1px 1px rgba(0,0,0,0.1);
}
.widget-user .widget-user-header {
    padding: 20px;
    height: 120px;
    border-top-right-radius: 3px;
    border-top-left-radius: 3px;
}
.bg-yellow {
    background-color: #f39c12 !important;
}
.bg-blue {
    background-color: #0073b7 !important;
}
.bg-aqua {
    background-color: #00c0ef !important;
}
.bg-green {
    background-color: #00a65a !important;
}
.bg-red {
    background-color: #dd4b39 !important;
}
.widget-user .widget-user-username {
    margin-top: 0;
    margin-bottom: 5px;
    font-size: 25px;
    font-weight: 300;
    text-shadow: 0 1px 1px rgba(0,0,0,0.2);
    color:#fff;
}
.widget-user .widget-user-desc {
    margin-top: 0;
    color:#fff;
	font-size: 13px;
}

.widget-user .widget-user-image {
    position: absolute;
    top: 65px;
    left: 50%;
    margin-left: -45px;
}
.widget-user .widget-user-image>img {
    width: 90px;
    height: auto;
    border: 3px solid #fff;
}
.widget-user .box-footer {
    padding-top: 30px;
}
.box-footer {
    border-top-left-radius: 0;
    border-top-right-radius: 0;
    border-bottom-right-radius: 3px;
    border-bottom-left-radius: 3px;
    border-top: 1px solid #f4f4f4;
    padding: 10px;
    background-color: #fff;
}
.box .border-right {
    border-right: 1px solid #f4f4f4;
}
.description-block {
    display: block;
    margin: 10px 0;
    text-align: center;
}
.description-block>.description-header {
    margin: 0;
    padding: 0;
    font-weight: 600;
    font-size: 16px;
}
.description-block>.description-text {
    text-transform: uppercase;
}
</style>
@endsection
