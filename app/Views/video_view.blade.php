@extends('layouts.web')

@section('page_title')
BLANK
@endsection



@section('content')
<!-- First Container -->
  <div class="container">

	<div class="row">  
		
		<div class="col-sm-8 text-center">
			@php
			//Optional parameters to be appended to embed
			$params = [
				'autoplay' => 1,
				'loop' => 1
			  ];

			//Optional attributes for embed container
			$attributes = [
			  'type' => null,
			  'class' => 'iframe-class',
			  'data-html5-parameter' => true
			];
			@endphp
		
			<div class="videoWrapper">
				{!! LaravelVideoEmbed::parse($data->video_url,'',$params,$attributes) !!}
			</div>
			
			<h3> @{{Komentar}} </h3>
			
		</div>
		<div class="col-sm-4 text-center">
		
			<a href="" class="btn btn-success btn-lg btn-block"> Berikan Sawer </a>
			<h3> @{{Creator}} </h3>
			<h3> @{{Video Lainnya dari kreator}} </h3>
		
		</div>
		
	</div>
  
</div>

@endsection

@section('css')
<style>
.videoWrapper {
  position: relative;
  padding-bottom: 56.25%; /* 16:9 */
  height: 0;
}
.videoWrapper iframe {
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
}
</style>
@endsection