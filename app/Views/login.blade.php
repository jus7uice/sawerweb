@extends('layouts.web')

@section('page_title')
Login
@endsection

@section('content')
<!-- First Container -->
<div class="container">
  
  
	
		<p class="login-box-msg">Login</p>
		
		
		 <form action="{{url('login')}}" method="post">
			<div class="form-group">
				<label for="exampleInputEmail1">Email *</label>
				<input type="email" class="form-control" name="email" placeholder="Email" value="{{old('email')}}" required>
			</div>
			
			<div class="form-group">
				<label for="exampleInputPassword1">Password *</label>
				<input type="password" class="form-control" placeholder="Password" name="password" required />
			</div>
			
			{{csrf_field()}}
			<button type="submit" class="btn btn-default">Login</button>
		</form>

		<!--
		<p>- OR -</p>

		<a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google"></i> Sign in using
		Google<a>
		<!-- /.social-auth-links -->

		<a href="#">Lupa Kata Kunci</a><br>
		Belum punya akun? <a href="{{url('/register')}}" class="text-center">Daftar</a>
  
  
  
</div>

@endsection