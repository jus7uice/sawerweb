<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;


class User extends Authenticatable
{
	use SoftDeletes;
    use Notifiable;
	
	protected $guarded = [];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $fillable = [
        // 'name', 'email', 'password', 'status'
    // ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
	
	
	public function organizer()
    {
        // return 'Admin Group';
		return $this->belongsTo('\App\Organizer', 'organizer_id');
    }
	
	public function post()
    {
        return $this->hasMany('\App\Post', 'user_id');
    }
	
	
}
