<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Post extends Model
{
	use SoftDeletes;
    protected $guarded = [];
	
	public function user()
    {
        // return 'Admin Group';
		return $this->belongsTo('\App\User', 'user_id');
    }
	
	public function category()
    {
        // return 'Admin Group';
		return $this->belongsTo('\App\Category', 'category_id');
    }
	
	public function channel()
    {
        // return 'Admin Group';
		return $this->belongsTo('\App\Channel', 'channel_id');
    }
	
	
}
