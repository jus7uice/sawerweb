<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminGroup extends Model
{
    //
	protected $table = 'admin_group';
	protected $guarded = [''];
	
	// protected $fillable = [
        // 'name', 'params'
    // ];
}
